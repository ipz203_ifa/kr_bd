﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EditPlayer.aspx.cs" Inherits="EditPlayer" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>EditPlayer</title>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous" />
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
</head>
<body>
     <div class="container">
    <nav class="navbar navbar-dark bg-dark justify-content-between">
      <a class="navbar-brand mx-5" style="color: white; font-size: xx-large; font-weight: bold" href="Index">Don't Starve Together</a>
    </nav>
       <br />
       <nav aria-label="breadcrumb">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="Index">Головна</a></li>
                    <li class="breadcrumb-item"><a href="PlayerView">Гравці</a></li>
                    <li class="breadcrumb-item active" aria-current="AddPlayer">Редагувати гравця</li>
                  </ol>
                </nav>
       <br />

        <div class="col-12">

    <form id="form1" runat="server">
        <div class="form-group col-6">
                                                            <% if (Context.GetOwinContext().Authentication.User.Identity.IsAuthenticated == true) { %>

                <h3>Редагувати дані про гравця</h3>
                <br />

            Ім'я персонажу
            <asp:TextBox ID="TextBox1" runat="server" required="required" class="form-control"></asp:TextBox>
            <br />
            Рівень
            <asp:TextBox ID="TextBox2" runat="server" class="form-control"></asp:TextBox>
            <br />
            Підлокація
            <asp:DropDownList ID="TextBox4" class="form-select" runat="server" DataSourceID="SqlDataSource1" DataTextField="nameSubLocation" DataValueField="idSubLocation"></asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DSTConnectionString %>" SelectCommand="SELECT [idSubLocation], [nameSubLocation] FROM [SubLocations]" OnInit="SqlDataSource1_Init"></asp:SqlDataSource>
            <br />
            Персонаж
            <asp:DropDownList ID="TextBox5" class="form-select" runat="server" DataSourceID="SqlDataSource2" DataTextField="nameHero" DataValueField="idHero"></asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:DSTConnectionString %>" SelectCommand="SELECT [nameHero], [idHero] FROM [Heros]"></asp:SqlDataSource>
            <br />
            Поточний квест
            <asp:DropDownList ID="TextBox6" class="form-select" runat="server" DataSourceID="SqlDataSource3" DataTextField="nameQuest" DataValueField="idQuest"></asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:DSTConnectionString %>" SelectCommand="SELECT [nameQuest], [idQuest] FROM [Quests]"></asp:SqlDataSource>
            <br />
            <br />
            <asp:Button ID="Button1" class="btn btn-dark" runat="server" OnClick="Button1_Click" Text="Змінити" />
        <%} %>
            <% else { %>
             <h3 class="alert alert-danger">Недостатньо прав</h3>
            <%} %>
        </div>
               
    </form>
            </div>
    </body>
</html>
