﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ItemsView.aspx.cs" Inherits="Pages_ItemsView" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Items</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous" />
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
</head>
<body>
    <div class="container">
    <nav class="navbar navbar-dark bg-dark justify-content-between">
      <a class="navbar-brand mx-5" style="color: white; font-size: xx-large; font-weight: bold" href="Index">Don't Starve Together</a>
    </nav>
        <br />
       <nav aria-label="breadcrumb">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="Index">Головна</a></li>
                    <li class="breadcrumb-item active" aria-current="Items">Предмети</li>
                  </ol>
                </nav>
       <br />
        <div class="col-12">
    <form id="form1" runat="server">
    <h1>Предмети</h1>
        <br />

                <div class="input-group">
  <div class="form-outline">
    <asp:Textbox type="search" placeholder="Search" id="TextBox1" runat="server" class="form-control" Text="" />
  </div>
                 <asp:Button ID="Button1" class="btn btn-dark" runat="server" Text="Пошук" OnClick="Button1_Click" />
</div><br />
                                <asp:LoginView runat="server" ViewStateMode="Disabled"><LoggedInTemplate>

                <asp:Label ID="Label2" runat="server" Text="Доступні дії: "></asp:Label><br />
     <a href="AddItem.aspx" class="btn btn-dark">Додати новий предмет</a><br /><br />
                                    </LoggedInTemplate></asp:LoginView>
         <asp:Label ID="Label1" runat="server" Text="Оберіть локацію: "></asp:Label>
            <asp:DropDownList ID="DropDownList1" class="form-select" AutoPostBack="true" runat="server" DataSourceID="SqlDataSource2" DataTextField="nameLocation" DataValueField="nameLocation" AppendDataBoundItems="true">
            <asp:ListItem Text="Всі" Value="" />
        </asp:DropDownList>
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:DSTConnectionString %>" SelectCommand="SELECT [nameLocation], [idLocation] FROM [Locations]"></asp:SqlDataSource>
        <br />
        <asp:Label ID="Label3" runat="server" Text="Оберіть тип предмету: "></asp:Label>
            <asp:DropDownList ID="DropDownList2" class="form-select" AutoPostBack="True" runat="server" DataSourceID="SqlDataSource3" DataTextField="nameTypeItem" DataValueField="nameTypeItem" AppendDataBoundItems="True">
            <asp:ListItem Text="Всі" Value="" />
        </asp:DropDownList>
        <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:DSTConnectionString %>" SelectCommand="SELECT * FROM [TypesItem]"></asp:SqlDataSource>
        <br />

        <div>
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="10" DataKeyNames="idItem" DataSourceID="SqlDataSource1" ForeColor="Black" GridLines="Horizontal" AllowPaging="True" AllowSorting="True" ShowHeaderWhenEmpty="True" OnRowDeleting="GridView1_RowDeleting" OnRowEditing="GridView1_RowEditing" HorizontalAlign="Center">
                <Columns>
                    <asp:BoundField DataField="idItem" HeaderText="№" ReadOnly="True" SortExpression="idItem" />
                    <asp:BoundField DataField="nameItem" HeaderText="Назва" SortExpression="nameItem" />
                    <asp:BoundField DataField="nameTypeItem" HeaderText="Тип" SortExpression="nameTypeItem" />
                    <asp:BoundField DataField="nameLocation" HeaderText="Локація" SortExpression="nameLocation" />
                    <asp:BoundField DataField="descItem" HeaderText="Опис" SortExpression="descItem" />
                    <asp:BoundField DataField="weightItem" HeaderText="Вага" SortExpression="weightItem" />
                    <asp:BoundField DataField="costItem" HeaderText="Вартість" SortExpression="costItem" />
                    <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
                </Columns>
                <FooterStyle BackColor="#333333" ForeColor="Black" />
                <HeaderStyle BackColor="#28242c" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="White" ForeColor="Black" Font-Size="X-Large" HorizontalAlign="Center" />
                <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#E5E5E5" />
                <SortedAscendingHeaderStyle BackColor="#333333" />
                <SortedDescendingCellStyle BackColor="#E5E5E5" />
                <SortedDescendingHeaderStyle BackColor="#333333" />
            </asp:GridView>
            <asp:SqlDataSource ID="SqlDataSource1" CancelSelectOnNullParameter="False" runat="server" ConnectionString="<%$ ConnectionStrings:DSTConnectionString %>" SelectCommand="SELECT * FROM [StructItems] WHERE (([nameItem] LIKE '%' + @nameItem + '%') AND ([nameLocation] LIKE '%' + @nameLocation + '%') AND ([nameTypeItem] LIKE '%' + @nameTypeItem + '%'))" InsertCommand="INSERT INTO [StructItems] ([idItem], [nameItem], [nameTypeItem], [nameLocation], [descItem], [weightItem], [costItem]) VALUES (@idItem, @nameItem, @nameTypeItem, @nameLocation, @descItem, @weightItem, @costItem)">
                <InsertParameters>
                    <asp:Parameter Name="idItem" Type="Int32" />
                    <asp:Parameter Name="nameItem" Type="String" />
                    <asp:Parameter Name="nameTypeItem" Type="String" />
                    <asp:Parameter Name="nameLocation" Type="String" />
                    <asp:Parameter Name="descItem" Type="String" />
                    <asp:Parameter Name="weightItem" Type="Double" />
                    <asp:Parameter Name="costItem" Type="Double" />
                </InsertParameters>
                <SelectParameters>
                    <asp:ControlParameter ControlID="TextBox1" DefaultValue="%" Name="nameItem" PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="DropDownList1" DefaultValue="%" Name="nameLocation" PropertyName="SelectedValue" Type="String" />
                    <asp:ControlParameter ControlID="DropDownList2" DefaultValue="%" Name="nameTypeItem" PropertyName="SelectedValue" Type="String" />
                </SelectParameters>
                        
            </asp:SqlDataSource>
        </div><br /><br />
               
    </form>
                        </div></div>
</body>
</html>
