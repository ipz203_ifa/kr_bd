﻿using Microsoft.AspNet.Identity;
using System;
using System.Linq;
using System.Web.UI;
using CourseWork;
using System.Data.SqlClient;

public partial class Account_Register : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Label5.Visible = false;
    }
    protected void Unnamed9_Click(object sender, EventArgs e)
    {
        var manager = new UserManager();
        var user = new ApplicationUser() { UserName = UserName.Text };
        IdentityResult result = manager.Create(user, Password.Text);
        if (result.Succeeded)
        {
            IdentityHelper.SignIn(manager, user, isPersistent: false);
            System.Data.SqlClient.SqlConnection sqlConnection = new System.Data.SqlClient.SqlConnection(@"Data Source = FRANC; Initial Catalog = DST; Integrated Security = True");

            System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.CommandText = "exec sp_addlogin @UserName,  @Password; exec sp_adduser @UserName, @UserName; exec sp_addrolemember 'db_datareader', @UserName";
            cmd.Connection = sqlConnection;
            sqlConnection.Open();

            SqlParameter UserNamep = new SqlParameter("@UserName", UserName.Text);
            SqlParameter Passwordp = new SqlParameter("@Password", Password.Text);
            cmd.Parameters.Add(UserNamep);
            cmd.Parameters.Add(Passwordp);
            cmd.ExecuteNonQuery();

            sqlConnection.Close();
            Label5.CssClass = "alert alert-success";
            Label5.Text = "Successfully Registered!";
            Label5.Visible = true;
            Response.Redirect("~/Index.aspx");
        }
        else
        {
            Label5.Visible = true;
            Label5.CssClass = "alert alert-danger";
            Label5.Text = result.Errors.FirstOrDefault();
        }
    }
}