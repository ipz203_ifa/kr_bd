﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using CourseWork;
using System.Data.SqlClient;
using Microsoft.Owin.Security;

public partial class Account_Manage : System.Web.UI.Page
{
    protected string SuccessMessage
    {
        get;
        private set;
    }

    protected bool CanRemoveExternalLogins
    {
        get;
        private set;
    }

    private bool HasPassword(UserManager manager)
    {
        var user = manager.FindById(User.Identity.GetUserId());
        return (user != null && user.PasswordHash != null);
    }

    protected void Page_Load()
    {
        if (!IsPostBack)
        {
            // Определите разделы для отображения
            UserManager manager = new UserManager();
            if (HasPassword(manager))
            {
                changePasswordHolder.Visible = true;
            }
            else
            {
                //setPassword.Visible = true;
                changePasswordHolder.Visible = false;
            }
            CanRemoveExternalLogins = manager.GetLogins(User.Identity.GetUserId()).Count() > 1;

            // Отобразить сообщение об успехе
            var message = Request.QueryString["m"];
            if (message != null)
            {
                // Извлечь строку запроса из действия
                Form.Action = ResolveUrl("~/Account/Manage");

                SuccessMessage =
                    message == "ChangePwdSuccess" ? "Ваш пароль змінено."
                    : message == "SetRoleSuccess" ? "Роль змінено."
                    : message == "RemoveLoginSuccess" ? "Обліковий запис видалено."
                    : String.Empty;
                successMessage.Visible = !String.IsNullOrEmpty(SuccessMessage);
            }
        }
    }

    protected void ChangePassword_Click(object sender, EventArgs e)
    {
        if (IsValid)
        {
            UserManager manager = new UserManager();
            IdentityResult result = manager.ChangePassword(User.Identity.GetUserId(), CurrentPassword.Text, NewPassword.Text);
            if (result.Succeeded)
            {
                var user = manager.FindById(User.Identity.GetUserId());
                IdentityHelper.SignIn(manager, user, isPersistent: false);
                System.Data.SqlClient.SqlConnection sqlConnection = new System.Data.SqlClient.SqlConnection(@"Data Source = FRANC; Initial Catalog = DST; Integrated Security = True");

                System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.CommandText = "exec sp_dropuser @UserName; exec sp_droplogin @UserName; ";
                cmd.Connection = sqlConnection;
                sqlConnection.Open();

                SqlParameter UserNamep = new SqlParameter("@UserName", User.Identity.GetUserName());
                cmd.Parameters.Add(UserNamep);
                cmd.ExecuteNonQuery();

                sqlConnection.Close();

                System.Data.SqlClient.SqlCommand cmd2 = new System.Data.SqlClient.SqlCommand();
                cmd2.CommandType = System.Data.CommandType.Text;
                cmd2.CommandText = "exec sp_addlogin @UserName,  @Password; exec sp_adduser @UserName, @UserName; exec sp_addrolemember 'db_datareader', @UserName";
                cmd2.Connection = sqlConnection;
                sqlConnection.Open();

                SqlParameter UserNamep2 = new SqlParameter("@UserName", User.Identity.GetUserName());
                SqlParameter Passwordp = new SqlParameter("@Password", NewPassword.Text);
                cmd2.Parameters.Add(UserNamep2);
                cmd2.Parameters.Add(Passwordp);
                cmd2.ExecuteNonQuery();

                sqlConnection.Close();
                Response.Redirect("~/Account/Manage?m=ChangePwdSuccess");
            }
            else
            {
                AddErrors(result);
            }
        }
    }

    protected void ChangeRole_Click(object sender, EventArgs e)
    {
        if (IsValid)
        {
            UserManager manager = new UserManager();
            var user = manager.FindById(DropDownList1.SelectedValue);

            if (user != null)
            {
                manager.AddToRole(user.Id, "admin");
                Response.Redirect("~/Account/Manage?m=SetRoleSuccess");
            }
        }
    }

    protected void DeleteAccount_Click(object sender, EventArgs e)
    {
        if (IsValid)
        {
            UserManager manager = new UserManager();
            var result = manager.RemoveLogin(User.Identity.GetUserId(), new UserLoginInfo(User.Identity.GetUserName(), TextBox1.Text));
            string msg = String.Empty;
            if (result.Succeeded)
            {
                var user = manager.FindById(User.Identity.GetUserId());
                //IdentityHelper.SignIn(manager, user, isPersistent: false);
                IdentityHelper.SignOut();
                manager.DeleteAsync(user);

                System.Data.SqlClient.SqlConnection sqlConnection = new System.Data.SqlClient.SqlConnection(@"Data Source = FRANC; Initial Catalog = DST; Integrated Security = True");
                System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.CommandText = "exec sp_dropuser @UserName; exec sp_droplogin @UserName; ";
                cmd.Connection = sqlConnection;
                sqlConnection.Open();
                SqlParameter UserNamep = new SqlParameter("@UserName", User.Identity.GetUserName());
                cmd.Parameters.Add(UserNamep);
                cmd.ExecuteNonQuery();
                sqlConnection.Close();

                msg = "?m=RemoveLoginSuccess";

                Response.Redirect("~/Index.aspx");
            }
            else
            {
                AddErrors(result);
            }
        }
    }

    protected void SetPassword_Click(object sender, EventArgs e)
    {
        //if (IsValid)
        //{
        //    // Создание информации о локальном имени входа и связывание локальной учетной записи с пользователем
        //    UserManager manager = new UserManager();
        //    IdentityResult result = manager.AddPassword(User.Identity.GetUserId(), password.Text);
        //    if (result.Succeeded)
        //    {
        //        Response.Redirect("~/Account/Manage?m=SetPwdSuccess");
        //    }
        //    else
        //    {
        //        AddErrors(result);
        //    }
        //}
    }

    public IEnumerable<UserLoginInfo> GetLogins()
    {
        UserManager manager = new UserManager();
        var accounts = manager.GetLogins(User.Identity.GetUserId());
        CanRemoveExternalLogins = accounts.Count() > 1 || HasPassword(manager);
        return accounts;
    }

    public void RemoveLogin(string loginProvider, string providerKey)
    {
        UserManager manager = new UserManager();
        var result = manager.RemoveLogin(User.Identity.GetUserId(), new UserLoginInfo(loginProvider, providerKey));
        string msg = String.Empty;
        if (result.Succeeded)
        {
            var user = manager.FindById(User.Identity.GetUserId());
            IdentityHelper.SignIn(manager, user, isPersistent: false);
            msg = "?m=RemoveLoginSuccess";
        }
        Response.Redirect("~/Account/Manage" + msg);
    }

    private void AddErrors(IdentityResult result)
    {
        foreach (var error in result.Errors)
        {
            ModelState.AddModelError("", error);
        }
    }
}