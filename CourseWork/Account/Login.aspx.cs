﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System;
using System.Web;
using System.Web.UI;
using CourseWork;
using System.Data.SqlClient;

public partial class Account_Login : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Label5.Visible = false;
    }

    protected void Unnamed9_Click(object sender, EventArgs e)
    {
        if (IsValid)
        {
            // Validate the user password
            var manager = new UserManager();
            ApplicationUser user = manager.Find(UserName.Text, Password.Text);
            if (user != null)
            {
                IdentityHelper.SignIn(manager, user, true);
                System.Data.SqlClient.SqlConnection sqlConnection = new System.Data.SqlClient.SqlConnection(@"Data Source = FRANC; Initial Catalog = DST; Integrated Security = True");
                System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.CommandText = "exec GetUser @UserName, @Password";
                cmd.Connection = sqlConnection;
                sqlConnection.Open();
                SqlParameter UserNamep = new SqlParameter("@UserName", UserName.Text);
                SqlParameter Passwordp = new SqlParameter("@Password", Password.Text);
                cmd.Parameters.Add(UserNamep);
                cmd.Parameters.Add(Passwordp);
                SqlDataReader reader = cmd.ExecuteReader();
                int count = 0;
                while (reader.Read())
                {
                    count = (int)reader[0];
                }
                if (count == 1)
                {
                    Label5.Text = "Успішний вхід!";
                    Label5.CssClass = "alert alert-success";
                    Response.Redirect("~/Index.aspx");
                }
                else
                {
                    Label5.Text = "Хм..Щось сталось, перевірте свої дані!";
                    Label5.CssClass = "alert alert-danger";
                }
                sqlConnection.Close();
                Label5.Visible = true;
            }
            else
            {
                Label5.CssClass = "alert alert-danger";
                Label5.Text = "Invalid username or password.";
                Label5.Visible = true;
            }
        }
    }
}