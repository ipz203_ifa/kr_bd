﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Manage.aspx.cs" Inherits="Account_Manage" %>

<%@ Register Src="~/Account/OpenAuthProviders.ascx" TagPrefix="uc" TagName="OpenAuthProviders" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Manage</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous" />
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
</head>
<body>
        <div class="container">
            <div class="col-12">
    <form id="form1" runat="server">
    <nav class="navbar navbar-dark bg-dark justify-content-between">
      <a class="navbar-brand mx-5" style="color: white; font-size: xx-large; font-weight: bold" href="Index">Don't Starve Together</a>
    </nav>
        <br />
       <nav aria-label="breadcrumb">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/Index.aspx">Головна</a></li>
                    <li class="breadcrumb-item"><a href="/Other.aspx">Авторизація</a></li>
                    <li class="breadcrumb-item active" aria-current="Other">Управління акаунтом</li>
                  </ol>
                </nav>
       <br />
        <div class="col-12">
                                <h1>Управління акаунтом</h1><br />

    <div>
        <asp:PlaceHolder runat="server" ID="successMessage" Visible="false" ViewStateMode="Disabled">
            <p class="text-success"><%: SuccessMessage %></p>
        </asp:PlaceHolder>

    </div>

    <div class="row">
        <div class="col-md-12">
            <section id="passwordForm">

                <asp:PlaceHolder runat="server" ID="changePasswordHolder" Visible="false">
                    <p>Ви увйшли як: <strong><%: User.Identity.GetUserName() %></strong>.</p><br />
<div class="form-group">
                            <asp:ValidationSummary runat="server" ShowModelStateErrors="true" CssClass="text-danger" />
                            <asp:Label runat="server" ID="Label1" AssociatedControlID="TextBox1" CssClass="col-md-2 control-label">Поточний пароль</asp:Label>
                            <div class="col-md-10">
                                <asp:TextBox runat="server" ID="TextBox1" TextMode="Password" CssClass="form-control" />
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="TextBox1"
                                    CssClass="text-danger" ErrorMessage="Поле поточного пароля є обов'язковим."
                                    ValidationGroup="DeleteAccount" />
                            </div>
                        </div>                    <asp:Button runat="server" Text="Видалити акаунт" OnClick="DeleteAccount_Click" CssClass="btn btn-dark" ValidationGroup="DeleteAccount" />
                     <br />
                    <% if (Context.GetOwinContext().Authentication.User.IsInRole("admin") == true)
                        { %>
                    <br />
                            <asp:ValidationSummary runat="server" ShowModelStateErrors="true" CssClass="text-danger" />
                            <asp:Label runat="server" ID="Label2" AssociatedControlID="DropDownList1" CssClass="control-label">Додати роль адміна користувачу</asp:Label>
                            <div class="col-md-10">
<asp:DropDownList ID="DropDownList1" class="form-select" runat="server" DataSourceID="SqlDataSource1" DataTextField="UserName" DataValueField="Id">
            </asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection%>" SelectCommand="SELECT * FROM [AspNetUsers]"></asp:SqlDataSource>
                              <asp:RequiredFieldValidator runat="server" ControlToValidate="DropDownList1"
                                    CssClass="text-danger" ErrorMessage="Поле вибору користувача є обов'язковим."
                                    ValidationGroup="ChangeRole" />
                            </div>
                                          <asp:Button runat="server" Text="Змінити роль" OnClick="ChangeRole_Click" CssClass="btn btn-dark" ValidationGroup="ChangeRole" />
                     <br />
                    <%} %>
                    <div class="form-horizontal">                   
                        <br />
                        <h3>Форма зміну паролю</h3><br />
                        <asp:ValidationSummary runat="server" ShowModelStateErrors="true" CssClass="text-danger" />
                        <div class="form-group">
                            <asp:Label runat="server" ID="CurrentPasswordLabel" AssociatedControlID="CurrentPassword" CssClass="col-md-2 control-label">Поточний пароль</asp:Label>
                            <div class="col-md-10">
                                <asp:TextBox runat="server" ID="CurrentPassword" TextMode="Password" CssClass="form-control" />
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="CurrentPassword"
                                    CssClass="text-danger" ErrorMessage="Поле поточного пароля є обов'язковим."
                                    ValidationGroup="ChangePassword" />
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" ID="NewPasswordLabel" AssociatedControlID="NewPassword" CssClass="col-md-2 control-label">Новий пароль</asp:Label>
                            <div class="col-md-10">
                                <asp:TextBox runat="server" ID="NewPassword" TextMode="Password" CssClass="form-control" />
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="NewPassword"
                                    CssClass="text-danger" ErrorMessage="Поле нового пароля є обов'язковим."
                                    ValidationGroup="ChangePassword" />
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" ID="ConfirmNewPasswordLabel" AssociatedControlID="ConfirmNewPassword" CssClass="col-md-2 control-label">Підтвердження нового паролю</asp:Label>
                            <div class="col-md-10">
                                <asp:TextBox runat="server" ID="ConfirmNewPassword" TextMode="Password" CssClass="form-control" />
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="ConfirmNewPassword"
                                    CssClass="text-danger" Display="Dynamic" ErrorMessage="Поле підтвердження нового паролю є обов'язковим."
                                    ValidationGroup="ChangePassword" />
                                <asp:CompareValidator runat="server" ControlToCompare="NewPassword" ControlToValidate="ConfirmNewPassword"
                                    CssClass="text-danger" Display="Dynamic" ErrorMessage="Новий пароль і його підтвердження не співпадають."
                                    ValidationGroup="ChangePassword" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-2 col-md-10"><br />
                                <asp:Button runat="server" Text="Змінити пароль" OnClick="ChangePassword_Click" CssClass="btn btn-dark" ValidationGroup="ChangePassword" />
                            </div>
                        </div>
                    </div>
                </asp:PlaceHolder>
            </section>

        </div>
    </div>

                    <br /><br />
        <br />

    </form>
            </div>
    </div>
</body>
</html>
