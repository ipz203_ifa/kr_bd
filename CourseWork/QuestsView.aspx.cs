﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Default2 : System.Web.UI.Page
{
    System.Data.SqlClient.SqlConnection sqlConnection = new System.Data.SqlClient.SqlConnection(@"Data Source = FRANC; Initial Catalog = DST; Integrated Security = True");

    protected void Page_Load(object sender, EventArgs e)
    {
        SqlDataSource1.DataBind();
        GridView1.DataBind();
    }

    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
        cmd.CommandType = System.Data.CommandType.Text;
        var txt = GridView1.Rows[e.RowIndex].Cells[0].Text;
        cmd.CommandText = "DELETE FROM [Quests] WHERE idQuest = @idQuest";
        cmd.Connection = sqlConnection;
        sqlConnection.Open();
        SqlParameter idQuest = new SqlParameter("@idQuest", txt.ToString());
        cmd.Parameters.Add(idQuest);
        cmd.ExecuteNonQuery();
        sqlConnection.Close();
        SqlDataSource1.DataBind();
        GridView1.DataBind();
    }

    public static void MessageBox(System.Web.UI.Page page, string strMsg)
    {
        ScriptManager.RegisterClientScriptBlock(page, page.GetType(), "alertMessage", "alert('" + strMsg + "')", true);
    }

    protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var txt = GridView1.Rows[e.NewEditIndex].Cells[0].Text;
        
        Response.Redirect("EditQuest.aspx?id=" + txt.ToString());
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        SqlDataSource1.DataBind();
        GridView1.DataBind();
    }

    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void SqlDataSource1_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {

    }
}