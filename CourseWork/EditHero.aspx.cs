﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using System.Runtime.Remoting.Metadata.W3cXsd2001;

public partial class EditHero : System.Web.UI.Page
{
    System.Data.SqlClient.SqlConnection sqlConnection = new System.Data.SqlClient.SqlConnection(@"Data Source = FRANC; Initial Catalog = DST; Integrated Security = True");

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    void GetCurrentValues()
    {
        System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
        cmd.CommandText = "SELECT * FROM [Heros] WHERE idHero = @idHero";
        cmd.Connection = sqlConnection;
        sqlConnection.Open();
        SqlParameter idQuest = new SqlParameter("@idHero", Request.QueryString["id"].ToString());
        cmd.Parameters.Add(idQuest);
        SqlDataReader dr = cmd.ExecuteReader();
        if (dr.Read() == true)
        {
            TextBox1.Text = dr.GetSqlValue(1).ToString();
            TextBox2.Text = dr.GetSqlValue(2).ToString();
            TextBox4.Text = dr.GetSqlValue(3).ToString();
            TextBox5.SelectedValue = dr.GetSqlValue(4).ToString();
            TextBox6.SelectedValue = dr.GetSqlValue(5).ToString();
            TextBox7.Text = dr.GetSqlValue(6).ToString();
            TextBox8.Text = dr.GetSqlValue(7).ToString();
            TextBox9.Text = dr.GetSqlValue(8).ToString();
        }
        sqlConnection.Close();
    }

    public static void MessageBox(System.Web.UI.Page page, string strMsg)
    {
        ScriptManager.RegisterClientScriptBlock(page, page.GetType(), "alertMessage", "alert('" + strMsg + "')", true);
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
        cmd.CommandType = System.Data.CommandType.Text;
        cmd.CommandText = "UPDATE [Heros] SET nameHero = @nameHero, hpHero = @hpHero, expHero = @expHero, typeHero = @typeHero, classHero = @classHero, descHero = @descHero, idLocation = @idLocation, idPhotoHero = @idPhotoHero WHERE idHero = @idHero";
        cmd.Connection = sqlConnection;
        sqlConnection.Open();
        SqlParameter nameHero = new SqlParameter("@nameHero", TextBox1.Text);
        SqlParameter hpHero = new SqlParameter("@hpHero", TextBox2.Text);
        SqlParameter expHero = new SqlParameter("@expHero", TextBox4.Text);
        SqlParameter typeHero = new SqlParameter("@typeHero", TextBox5.SelectedItem.Value);
        SqlParameter classHero = new SqlParameter("@classHero", TextBox6.SelectedItem.Value);
        SqlParameter descHero = new SqlParameter("@descHero", TextBox7.Text);
        SqlParameter idLocation = new SqlParameter("@idLocation", TextBox8.SelectedItem.Value);
        SqlParameter idPhotoHero = new SqlParameter("@idPhotoHero", TextBox9.SelectedItem.Value);
        SqlParameter idHero = new SqlParameter("@idHero", Request.QueryString["id"]);
        cmd.Parameters.Add(nameHero);
        cmd.Parameters.Add(hpHero);
        cmd.Parameters.Add(expHero);
        cmd.Parameters.Add(typeHero);
        cmd.Parameters.Add(classHero);
        cmd.Parameters.Add(descHero);
        cmd.Parameters.Add(idLocation);
        cmd.Parameters.Add(idPhotoHero);
        cmd.Parameters.Add(idHero);
        cmd.ExecuteNonQuery();
        sqlConnection.Close();
        Response.Redirect("HerosView.aspx");
    }

    public byte[] ToBytes(string hex)
    {
        var shb = SoapHexBinary.Parse(hex.Remove(hex.Length - 1));

        return shb.Value;
    }

    protected void TextBox9_SelectedIndexChanged(object sender, EventArgs e)
    {
        System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
        cmd.CommandType = System.Data.CommandType.Text;
        cmd.CommandText = "SELECT [heroPictureData] FROM [HeroPicture] WHERE idHeroPicture = @idHeroPicture";
        cmd.Connection = sqlConnection;
        sqlConnection.Open();
        SqlParameter idHeroPicture = new SqlParameter("@idHeroPicture", TextBox9.SelectedItem.Value);
        cmd.Parameters.Add(idHeroPicture);
        byte[] yourImageByteArray;
        SqlDataReader dr = cmd.ExecuteReader();
        if (dr.Read() == true)
        {
            yourImageByteArray = dr.GetFieldValue<byte[]>(0);
            Image1.ImageUrl = "data:image/jpg;base64," + Convert.ToBase64String(yourImageByteArray);
        }
        sqlConnection.Close();
    }

    protected void SqlDataSource1_Init(object sender, EventArgs e)
    {
        GetCurrentValues();
    }
}