﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AddQuest : System.Web.UI.Page
{
    System.Data.SqlClient.SqlConnection sqlConnection = new System.Data.SqlClient.SqlConnection(@"Data Source = FRANC; Initial Catalog = DST; Integrated Security = True");

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
        cmd.CommandType = System.Data.CommandType.Text;
        cmd.CommandText = "INSERT [Quests] (nameQuest, idSubLocation, idHero, conditionQuest, typeQuest, rewardExpQuest, " +
            "rewardMoneyQuest, recommendedLevelQuest, goalsQuest) " +
            "VALUES(@nameQuest, @idSubLocation, @idHero, @conditionQuest, @typeQuest, @rewardExpQuest, @rewardMoneyQuest," +
            " @recommendedLevelQuest, @goalsQuest)";
        cmd.Connection = sqlConnection;
        sqlConnection.Open();
        SqlParameter nameQuest = new SqlParameter("@nameQuest", TextBox1.Text);
        SqlParameter idSubLocation = new SqlParameter("@idSubLocation", TextBox2.SelectedItem.Value);
        SqlParameter idHero = new SqlParameter("@idHero", TextBox4.SelectedItem.Value);
        SqlParameter conditionQuest = new SqlParameter("@conditionQuest", TextBox5.Text);
        SqlParameter typeQuest = new SqlParameter("@typeQuest", TextBox6.SelectedItem.Value);
        SqlParameter rewardExpQuest = new SqlParameter("@rewardExpQuest", TextBox7.Text);
        SqlParameter rewardMoneyQuest = new SqlParameter("@rewardMoneyQuest", TextBox8.Text);
        SqlParameter recommendedLevelQuest = new SqlParameter("@recommendedLevelQuest", TextBox9.Text);
        SqlParameter goalsQuest = new SqlParameter("@goalsQuest", TextBox10.Text);
        cmd.Parameters.Add(nameQuest);
        cmd.Parameters.Add(idSubLocation);
        cmd.Parameters.Add(idHero);
        cmd.Parameters.Add(conditionQuest);
        cmd.Parameters.Add(typeQuest);
        cmd.Parameters.Add(rewardExpQuest);
        cmd.Parameters.Add(rewardMoneyQuest);
        cmd.Parameters.Add(recommendedLevelQuest);
        cmd.Parameters.Add(goalsQuest);
        cmd.ExecuteNonQuery();
        sqlConnection.Close();
        Response.Redirect("QuestsView.aspx");
    }
}