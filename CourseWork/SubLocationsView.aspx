﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SubLocationsView.aspx.cs" Inherits="Pages_SubLocationsView" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>SubLocations</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous" />
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
</head>
<body>
    <div class="container">
    <nav class="navbar navbar-dark bg-dark justify-content-between">
      <a class="navbar-brand mx-5" style="color: white; font-size: xx-large; font-weight: bold" href="Index">Don't Starve Together</a>
    </nav>
        <br />
       <nav aria-label="breadcrumb">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="Index">Головна</a></li>
                    <li class="breadcrumb-item active" aria-current="Підлокації">Підлокації</li>
                  </ol>
                </nav>
       <br />
        <div class="col-12">
    <form runat="server">

                    <h1>Підлокації</h1><br />

             <div class="input-group">
  <div class="form-outline">
    <asp:Textbox type="search" placeholder="Search" id="TextBox1" runat="server" class="form-control" Text="" />
  </div>
                 <asp:Button ID="Button1" class="btn btn-dark" runat="server" Text="Пошук" OnClick="Button1_Click" />
</div><br />
                <asp:LoginView runat="server" ViewStateMode="Disabled">

       <LoggedInTemplate>

             <asp:Label ID="Label2" runat="server" Text="Доступні дії: "></asp:Label><br />
        <a href="AddSubLocation.aspx" class="btn btn-dark">Додати нову підлокацію</a>
        <a href="AddLocation.aspx" class="btn btn-dark">Додати нову локацію</a>
     <a href="AddPictureLocation.aspx" class="btn btn-dark">Додати нову мапу локації</a><br /><br />
           </LoggedInTemplate>
                                </asp:LoginView>

        <asp:Label ID="Label1" runat="server" Text="Оберіть локацію: "></asp:Label>
        <div>
            <asp:DropDownList ID="DropDownList1" class="form-select" AutoPostBack="true" runat="server" DataSourceID="SqlDataSource2" DataTextField="nameLocation" DataValueField="nameLocation" AppendDataBoundItems="true" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
            <asp:ListItem Text="Всі" Value="%" Selected="True" />
        </asp:DropDownList>
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:DSTConnectionString %>" SelectCommand="SELECT [idLocation], [nameLocation] FROM [Locations]"></asp:SqlDataSource>
        <br />
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" BackColor="White" CellPadding="10" DataSourceID="SqlDataSource1" ForeColor="Black" GridLines="Horizontal" DataKeyNames="idSubLocation" OnRowDeleting="GridView1_RowDeleting" OnRowDataBound="GridView1_RowDataBound" OnRowEditing="GridView1_RowEditing" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" HorizontalAlign="Center" AllowSorting="True" AllowPaging="True" ShowHeaderWhenEmpty="True">
                <Columns>
                    <asp:BoundField DataField="idSubLocation" HeaderText="№" SortExpression="idSubLocation" ReadOnly="True" >
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                    </asp:BoundField>
                    <asp:BoundField DataField="nameSubLocation" HeaderText="Назва" SortExpression="nameSubLocation" >
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                    </asp:BoundField>
                    <asp:BoundField DataField="nameLocation" HeaderText="Локація" SortExpression="nameLocation" >
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="Мапа">
                        <ItemTemplate>
                          <img src='<%# Eval("locationPictureData") %>' id="imageControl" runat="server" width="200" />
                        </ItemTemplate>   
                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                    </asp:TemplateField> 
                    <asp:BoundField DataField="descSubLocation" HeaderText="Опис" SortExpression="descSubLocation" >
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                    <ItemStyle HorizontalAlign="Justify" VerticalAlign="Middle" />
                    </asp:BoundField>
                    
                    <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" >
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                    </asp:CommandField>
                    
                </Columns>
                <FooterStyle BackColor="#333333" ForeColor="Black" />
                <HeaderStyle BackColor="#28242c" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="White" ForeColor="Black" Font-Size="X-Large" HorizontalAlign="Center" />
                <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#E5E5E5" />
                <SortedAscendingHeaderStyle BackColor="#333333" />
                <SortedDescendingCellStyle BackColor="#E5E5E5" />
                <SortedDescendingHeaderStyle BackColor="#333333" />
                
            </asp:GridView>
            <br />
        </div>
        <asp:SqlDataSource ID="SqlDataSource1"  runat="server" CancelSelectOnNullParameter="False" ConnectionString="<%$ ConnectionStrings:DSTConnectionString %>" SelectCommand="SELECT * FROM [StructSubLocations] WHERE (([nameLocation] LIKE '%' + @nameLocation + '%') AND ([nameSubLocation] LIKE '%' + @nameSubLocation + '%'))">
            <SelectParameters>
                <asp:ControlParameter ControlID="DropDownList1" Name="nameLocation" PropertyName="SelectedValue" Type="String" DefaultValue="%" />
                <asp:ControlParameter ControlID="TextBox1" DefaultValue="%" Name="nameSubLocation" PropertyName="Text" Type="String" />
            </SelectParameters>
        </asp:SqlDataSource><br /><br />
         </form>
    </div>
    </div>
</body>
</html>
