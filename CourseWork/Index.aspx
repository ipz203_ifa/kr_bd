﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Index.aspx.cs" Inherits="Index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Don't Starve Together</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous" />
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
</head>
<body>
        <div class="container">
            <div class="col-12">
    <form id="form1" runat="server">
    <nav class="navbar navbar-dark bg-dark justify-content-between">
      <a class="navbar-brand mx-5" style="color: white; font-size: xx-large; font-weight: bold" href="Index">Don't Starve Together</a>
    </nav>
        <br />

        <asp:Image ID="Image1" runat="server" ImageUrl="~/Img/banner.png" Width="1297px" Height="250px" ImageAlign="Middle" />
        <br /><br />
        <br /><br />
<div class="card-group">
  <div class="card">
    <asp:Image  runat="server" Height="600" class="card-img-top" ImageUrl="~/Img/map.png" alt="Card image cap" />
    <div class="card-body">
      <a class="card-title link-dark" href="SubLocationsView.aspx"><h5>Підлокації мапи</h5></a>
      <p class="card-text">Мапа є важливим елементом ігрового процесу. Вона генерується біомами випадковим чином кожного разу при створенні нового світу, що забезпечує унікальний досвід для гравців кожного разу.</p>
    </div>
  </div>
 
        <div class="card-group">
              <div class="card">
    <asp:Image  runat="server" Height="500" class="card-img-top" ImageUrl="~/Img/persons.png" alt="Card image cap" />
    <div class="card-body">
      <a class="card-title link-dark" href="HerosView.aspx"><h5>Персонажі</h5></a>
      <p class="card-text">У грі «Don't Starve Together» доступно безліч унікальних персонажів, кожен із яких має свої особливості, навички та слабкості. Кожен персонаж додає до гри свої унікальні механіки, що впливають на стиль гри.</p>
    </div>
  </div>
  <div class="card">
    <asp:Image  runat="server" Height="500" class="card-img-top" ImageUrl="~/Img/quests.png" alt="Card image cap" />
    <div class="card-body">
      <a class="card-title link-dark" href="QuestsView.aspx"><h5>Квести</h5></a>
      <p class="card-text">У «Don't Starve Together» квести не є структурованими місіями, як у багатьох інших іграх, а більше нагадують завдання або цілі, які гравці можуть виконувати для виживання та прогресу.</p>
    </div>
  </div>
</div><br />
  <div class="card">
    <asp:Image  runat="server" Height="300" class="card-img-top" ImageUrl="~/Img/items.png" alt="Card image cap" />
    <div class="card-body">
      <a class="card-title link-dark" href="ItemsView.aspx"><h5>Предмети</h5></a>
      <p class="card-text">Предмети відіграють ключову роль у виживанні та прогресі гравців. Вони поділяються на різні категорії, кожна з яких має свої особливості та призначення.</p>
    </div>
  </div>
  <div class="card">
    <asp:Image  runat="server" Height="300" class="card-img-top" ImageUrl="~/Img/Heros/Wagstaff_portrait.png" alt="Card image cap" />
    <div class="card-body">
      <a class="card-title link-dark" href="PlayerView.aspx"><h5>Гравець</h5></a>
      <p class="card-text">Сторінка з інформацією про гравця, його досягнення в грі, статистика гри.</p>
    </div>
  </div>
  <div class="card">
    <asp:Image  runat="server" Height="300" class="card-img-top" ImageUrl="~/Img/logo.png" alt="Card image cap" />
    <div class="card-body">
      <a class="card-title link-dark" href="Other.aspx"><h5>Авторизація</h5></a>
      <p class="card-text">Сторінка авторизації для роботи з базою даних.</p>
    </div>
  </div>
</div>
        <br />
        <br />
                
    </form>
            </div>
    </div>
</body>
</html>

