﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AddItem.aspx.cs" Inherits="AddItem" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>NewItem</title>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous" />
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
</head>
<body>
     <div class="container">
    <nav class="navbar navbar-dark bg-dark justify-content-between">
      <a class="navbar-brand mx-5" style="color: white; font-size: xx-large; font-weight: bold" href="Index">Don't Starve Together</a>
    </nav>
       <br />
       <nav aria-label="breadcrumb">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="Index">Головна</a></li>
                    <li class="breadcrumb-item"><a href="ItemsView">Предмети</a></li>
                    <li class="breadcrumb-item active" aria-current="AddItem">Додати новий предмет</li>
                  </ol>
                </nav>
       <br />

        <div class="col-12">

    <form id="form1" runat="server">
        <div class="form-group col-6">
             <% if (Context.GetOwinContext().Authentication.User.Identity.IsAuthenticated == true)
                { %>
    <h3>Додати предмет</h3><br />
            Назва предмету
            <asp:TextBox ID="TextBox1" runat="server" required="required" class="form-control"></asp:TextBox>
            <br />
            Тип предмету
            <asp:DropDownList ID="TextBox2" class="form-select" runat="server" DataSourceID="SqlDataSource1" DataTextField="nameTypeItem" DataValueField="idTypeItem">
            </asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DSTConnectionString %>" SelectCommand="SELECT [idTypeItem], [nameTypeItem] FROM [TypesItem]"></asp:SqlDataSource>
            <br />
           Локація
            <asp:DropDownList ID="TextBox4" class="form-select" runat="server" DataSourceID="SqlDataSource2" DataTextField="nameLocation" DataValueField="idLocation"></asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:DSTConnectionString %>" SelectCommand="SELECT [nameLocation], [idLocation] FROM [Locations]"></asp:SqlDataSource>
            <br />
           Опис
            <asp:TextBox ID="TextBox5" runat="server" class="form-control"></asp:TextBox>
            <br />
            Вага
            <asp:TextBox ID="TextBox6" runat="server" class="form-control"></asp:TextBox>
            <br />
            Вартість
            <asp:TextBox ID="TextBox7" runat="server" class="form-control"></asp:TextBox>
            <br />
            <br />
            <asp:Button ID="Button1" class="btn btn-dark" runat="server" OnClick="Button1_Click" Text="Додати" />
        <%} %>
            <% else { %>
             <h3 class="alert alert-danger">Недостатньо прав</h3>
            <%} %>
        </div>
        <br /><br />
               
    </form> </div> </div>
</body>
</html>
