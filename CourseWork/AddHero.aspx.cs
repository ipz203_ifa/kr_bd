﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using System.Runtime.Remoting.Metadata.W3cXsd2001;

public partial class AddHero : System.Web.UI.Page
{
    System.Data.SqlClient.SqlConnection sqlConnection = new System.Data.SqlClient.SqlConnection(@"Data Source = FRANC; Initial Catalog = DST; Integrated Security = True");

    protected void Page_Load(object sender, EventArgs e)
    {
        if(TextBox9.SelectedValue == "")
        {
            Image1.ImageUrl = "~/Img/Heros/default.jpg";
        }
    }

    public static void MessageBox(System.Web.UI.Page page, string strMsg)
    {
        ScriptManager.RegisterClientScriptBlock(page, page.GetType(), "alertMessage", "alert('" + strMsg + "')", true);
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
        cmd.CommandType = System.Data.CommandType.Text;
        cmd.CommandText = "INSERT [Heros] (nameHero, hpHero, expHero, typeHero, classHero, descHero, idLocation, idPhotoHero) " +
            "VALUES(@nameHero, @hpHero, @expHero, @typeHero, @classHero, @descHero, @idLocation, @idPhotoHero)";
        cmd.Connection = sqlConnection;
        sqlConnection.Open();
        SqlParameter nameHero = new SqlParameter("@nameHero", TextBox1.Text);
        SqlParameter hpHero = new SqlParameter("@hpHero", TextBox2.Text);
        SqlParameter expHero = new SqlParameter("@expHero", TextBox4.Text);
        SqlParameter typeHero = new SqlParameter("@typeHero", TextBox5.SelectedItem.Value);
        SqlParameter classHero = new SqlParameter("@classHero", TextBox6.SelectedItem.Value);
        SqlParameter descHero = new SqlParameter("@descHero", TextBox7.Text);
        SqlParameter idLocation = new SqlParameter("@idLocation", TextBox8.SelectedItem.Value);
        SqlParameter idPhotoHero = new SqlParameter("@idPhotoHero", TextBox9.SelectedItem.Value);
        cmd.Parameters.Add(nameHero);
        cmd.Parameters.Add(hpHero);
        cmd.Parameters.Add(expHero);
        cmd.Parameters.Add(typeHero);
        cmd.Parameters.Add(classHero);
        cmd.Parameters.Add(descHero);
        cmd.Parameters.Add(idLocation);
        cmd.Parameters.Add(idPhotoHero);
        cmd.ExecuteNonQuery();
        sqlConnection.Close();
        Response.Redirect("HerosView.aspx");
    }

    public byte[] ToBytes(string hex)
    {
        var shb = SoapHexBinary.Parse(hex.Remove(hex.Length - 1));

        return shb.Value;
    }

    protected void TextBox9_SelectedIndexChanged(object sender, EventArgs e)
    {
        System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
        cmd.CommandType = System.Data.CommandType.Text;
        cmd.CommandText = "SELECT [heroPictureData] FROM [HeroPicture] WHERE idHeroPicture = @idHeroPicture";
        cmd.Connection = sqlConnection;
        sqlConnection.Open();
        SqlParameter idHeroPicture = new SqlParameter("@idHeroPicture", TextBox9.SelectedItem.Value);
        cmd.Parameters.Add(idHeroPicture);
        byte[] yourImageByteArray;
        SqlDataReader dr = cmd.ExecuteReader();
        if (dr.Read() == true)
        {
            yourImageByteArray =  dr.GetFieldValue<byte[]>(0);
            Image1.ImageUrl = "data:image/jpg;base64," + Convert.ToBase64String(yourImageByteArray);
        }
        sqlConnection.Close();
    }
}