﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="QuestsView.aspx.cs" Inherits="Default2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Quests</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous" />
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
</head>
<body>
    <div class="container">
    <nav class="navbar navbar-dark bg-dark justify-content-between">
      <a class="navbar-brand mx-5" style="color: white; font-size: xx-large; font-weight: bold" href="Index">Don't Starve Together</a>
    </nav>
        <br />
       <nav aria-label="breadcrumb">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="Index">Головна</a></li>
                    <li class="breadcrumb-item active" aria-current="Player">Квести</li>
                  </ol>
                </nav>
       <br />
        <div class="col-12">
    <form id="form1" runat="server">
    <h1>Квести</h1>
        <br />

        <div class="input-group">
  <div class="form-outline">
    <asp:Textbox type="search" placeholder="Search" id="TextBox1" runat="server" class="form-control" Text="" />
  </div>
                 <asp:Button ID="Button1" class="btn btn-dark" runat="server" Text="Пошук" OnClick="Button1_Click" />
</div><br />
                                <asp:LoginView runat="server" ViewStateMode="Disabled"><LoggedInTemplate>

        <asp:Label ID="Label2" runat="server" Text="Доступні дії: "></asp:Label><br />
     <a href="AddQuest.aspx" class="btn btn-dark">Додати новий квест</a><br /><br />
                                    </LoggedInTemplate></asp:LoginView>
        <div>

            <asp:Label ID="Label1" runat="server" Text="Оберіть підлокацію: "></asp:Label>
            <asp:DropDownList ID="DropDownList1" class="form-select" AutoPostBack="true" runat="server" DataSourceID="SqlDataSource2" DataTextField="nameSubLocation" DataValueField="nameSubLocation" AppendDataBoundItems="true">
            <asp:ListItem Text="Всі" Value="%" />
        </asp:DropDownList>
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:DSTConnectionString %>" SelectCommand="SELECT [idSubLocation], [nameSubLocation] FROM [SubLocations]"></asp:SqlDataSource>
        <br />
        <asp:Label ID="Label3" runat="server" Text="Оберіть героя, що видав: "></asp:Label>
            <asp:DropDownList ID="DropDownList2" class="form-select" AutoPostBack="True" runat="server" DataSourceID="SqlDataSource3" DataTextField="nameHero" DataValueField="nameHero" AppendDataBoundItems="True">
            <asp:ListItem Text="Всі" Value="%" />
        </asp:DropDownList>
        <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:DSTConnectionString %>" SelectCommand="SELECT [nameHero], [idHero] FROM [Heros]"></asp:SqlDataSource>
        <br />
        <asp:Label ID="Label4" runat="server" Text="Оберіть тип квесту: "></asp:Label>
            <asp:DropDownList ID="DropDownList3" class="form-select" AutoPostBack="True" runat="server" DataSourceID="SqlDataSource4" DataTextField="nameTypeQuest" DataValueField="nameTypeQuest" AppendDataBoundItems="True">
            <asp:ListItem Text="Всі" Value="" />
        </asp:DropDownList>
        <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:DSTConnectionString %>" SelectCommand="SELECT [nameTypeQuest], [idTypeQuest] FROM [TypesQuests]"></asp:SqlDataSource>
        <br />

            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="10" DataKeyNames="idQuest" DataSourceID="SqlDataSource1" ForeColor="Black" GridLines="Horizontal" OnRowDeleting="GridView1_RowDeleting" OnRowEditing="GridView1_RowEditing" AllowPaging="True" AllowSorting="True" HorizontalAlign="Center" CellSpacing="10" ShowHeaderWhenEmpty="True" OnSelectedIndexChanged="GridView1_SelectedIndexChanged">
                <Columns>
                    <asp:BoundField DataField="idQuest" HeaderText="№" ReadOnly="True" SortExpression="idQuest" />
                    <asp:BoundField DataField="nameQuest" HeaderText="Назва" SortExpression="nameQuest" />
                    <asp:BoundField DataField="nameSubLocation" HeaderText="Підлокація" SortExpression="nameSubLocation" />
                    <asp:BoundField DataField="nameHero" HeaderText="Герой, що видав" SortExpression="nameHero" />
                    <asp:BoundField DataField="conditionQuest" HeaderText="Умова" SortExpression="conditionQuest" />
                    <asp:BoundField DataField="nameTypeQuest" HeaderText="Тип квесту" SortExpression="nameTypeQuest" />
                    <asp:BoundField DataField="rewardExpQuest" HeaderText="Очки досвіду" SortExpression="rewardExpQuest" />
                    <asp:BoundField DataField="rewardMoneyQuest" HeaderText="Монети" SortExpression="rewardMoneyQuest" />
                    <asp:BoundField DataField="recommendedLevelQuest" HeaderText="Рекомендований рівень" SortExpression="recommendedLevelQuest" />
                    <asp:BoundField DataField="goalsQuest" HeaderText="Завдання" SortExpression="goalsQuest" />
                    <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
                </Columns>
                <FooterStyle BackColor="#333333" ForeColor="Black" />
                <HeaderStyle BackColor="#28242c" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="White" ForeColor="Black" Font-Size="X-Large" HorizontalAlign="Center" />
                <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#E5E5E5" />
                <SortedAscendingHeaderStyle BackColor="#333333" />
                <SortedDescendingCellStyle BackColor="#E5E5E5" />
                <SortedDescendingHeaderStyle BackColor="#333333" />
            </asp:GridView>
        </div>
        <asp:SqlDataSource ID="SqlDataSource1" CancelSelectOnNullParameter="False" runat="server" ConnectionString="<%$ ConnectionStrings:DSTConnectionString %>" SelectCommand="SELECT * FROM [StructQuests] WHERE (([nameQuest] LIKE '%' + @nameQuest + '%') AND ([nameSubLocation] LIKE '%' + @nameSubLocation + '%') AND ([nameHero] LIKE '%' + @nameHero + '%') AND ([nameTypeQuest] LIKE '%' + @nameTypeQuest + '%'))" InsertCommand="INSERT INTO [StructQuests] ([idQuest], [nameQuest], [nameSubLocation], [nameHero], [conditionQuest], [nameTypeQuest], [rewardExpQuest], [rewardMoneyQuest], [recommendedLevelQuest], [goalsQuest]) VALUES (@idQuest, @nameQuest, @nameSubLocation, @nameHero, @conditionQuest, @nameTypeQuest, @rewardExpQuest, @rewardMoneyQuest, @recommendedLevelQuest, @goalsQuest)" OnSelecting="SqlDataSource1_Selecting">
            <InsertParameters>
                <asp:Parameter Name="idQuest" Type="Int32" />
                <asp:Parameter Name="nameQuest" Type="String" />
                <asp:Parameter Name="nameSubLocation" Type="String" />
                <asp:Parameter Name="nameHero" Type="String" />
                <asp:Parameter Name="conditionQuest" Type="String" />
                <asp:Parameter Name="nameTypeQuest" Type="String" />
                <asp:Parameter Name="rewardExpQuest" Type="Int32" />
                <asp:Parameter Name="rewardMoneyQuest" Type="Int32" />
                <asp:Parameter Name="recommendedLevelQuest" Type="Int32" />
                <asp:Parameter Name="goalsQuest" Type="String" />
            </InsertParameters>
            <SelectParameters>
                <asp:ControlParameter ControlID="TextBox1" DefaultValue="%" Name="nameQuest" PropertyName="Text" Type="String" />
                <asp:ControlParameter ControlID="DropDownList1" DefaultValue="%" Name="nameSubLocation" PropertyName="SelectedValue" Type="String" />
                <asp:ControlParameter ControlID="DropDownList2" DefaultValue="%" Name="nameHero" PropertyName="SelectedValue" Type="String" />
                <asp:ControlParameter ControlID="DropDownList3" DefaultValue="%" Name="nameTypeQuest" PropertyName="SelectedValue" Type="String" />
            </SelectParameters>
        </asp:SqlDataSource>
    <br /><br />
                   </form>
            </div></div>
</body>
</html>
