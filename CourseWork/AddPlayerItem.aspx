﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AddPlayerItem.aspx.cs" Inherits="AddPlayerItem" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>NewPlayerItem</title>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous" />
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
</head>
<body>
     <div class="container">
    <nav class="navbar navbar-dark bg-dark justify-content-between">
      <a class="navbar-brand mx-5" style="color: white; font-size: xx-large; font-weight: bold" href="Index">Don't Starve Together</a>
    </nav>
       <br />
       <nav aria-label="breadcrumb">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="Index">Головна</a></li>
                    <li class="breadcrumb-item"><a href="PlayerView">Гравці</a></li>
                    <li class="breadcrumb-item active" aria-current="AddItem">Додати новий предмет гравцю</li>
                  </ol>
                </nav>
       <br />

        <div class="col-12">

    <form id="form1" runat="server">
        <div class="form-group col-6">
      <% if (Context.GetOwinContext().Authentication.User.Identity.IsAuthenticated == true) { %>

    <h3>Додати предмет гравцю</h3><br />
            Гравець
            <asp:DropDownList ID="TextBox2" AutoPostBack="true" class="form-select" runat="server" DataSourceID="SqlDataSource1" DataTextField="namePlayer" DataValueField="idPlayer" OnSelectedIndexChanged="TextBox2_SelectedIndexChanged">
            </asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DSTConnectionString %>" SelectCommand="SELECT [namePlayer], [idPlayer] FROM [Player]"></asp:SqlDataSource>
            <br />
           Предмет
            <asp:DropDownList ID="TextBox4" class="form-select" runat="server" DataSourceID="SqlDataSource2" DataTextField="nameItem" DataValueField="idItem"></asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:DSTConnectionString %>" SelectCommand="SELECT [idItem], [nameItem] FROM [Items]"></asp:SqlDataSource>
            <br />
           Кількість
            <asp:TextBox ID="TextBox5" runat="server" class="form-control"></asp:TextBox>
            <br />
            <br />
            <asp:Button ID="Button1" class="btn btn-dark" runat="server" OnClick="Button1_Click" Text="Додати" />
        </div>
        <br /><br />
        <h3>Наявні предмети у гравця</h3>
        <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="10" DataKeyNames="idPlayerItem" DataSourceID="SqlDataSource3" ForeColor="Black" GridLines="Horizontal" ShowHeaderWhenEmpty="True">
            <Columns>
                <asp:BoundField DataField="idPlayerItem" HeaderText="№" ReadOnly="True" SortExpression="idPlayerItem" />
                <asp:BoundField DataField="namePlayer" HeaderText="Ім'я" SortExpression="namePlayer" />
                <asp:BoundField DataField="nameItem" HeaderText="Назва предмету" SortExpression="nameItem" />
                <asp:BoundField DataField="countPlayerItem" HeaderText="Кількість" SortExpression="countPlayerItem" />
            </Columns>
                <FooterStyle BackColor="#333333" ForeColor="Black" />
                <HeaderStyle BackColor="#28242c" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="White" ForeColor="Black" Font-Size="X-Large" HorizontalAlign="Center" />
                <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#E5E5E5" />
                <SortedAscendingHeaderStyle BackColor="#333333" />
                <SortedDescendingCellStyle BackColor="#E5E5E5" />
                <SortedDescendingHeaderStyle BackColor="#333333" />
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:DSTConnectionString %>" SelectCommand="SELECT [idPlayerItem], [namePlayer], [nameItem], [countPlayerItem] FROM [StructPlayerItems] WHERE ([idPlayer] = @idPlayer)">
            <SelectParameters>
                <asp:ControlParameter ControlID="TextBox2" Name="idPlayer" PropertyName="SelectedValue" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
        <%} %>
            <% else { %>
             <h3 class="alert alert-danger">Недостатньо прав</h3>
            <%} %>
        <br />
              
    </form> </div> </div>
</body>
</html>
