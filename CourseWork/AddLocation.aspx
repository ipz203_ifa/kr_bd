﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AddLocation.aspx.cs" Inherits="AddLocation" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>NewLocation</title>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous" />
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
</head>
<body>
     <div class="container">
    <nav class="navbar navbar-dark bg-dark justify-content-between">
      <a class="navbar-brand mx-5" style="color: white; font-size: xx-large; font-weight: bold" href="Index">Don't Starve Together</a>
    </nav>
       <br />
       <nav aria-label="breadcrumb">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="Index">Головна</a></li>
                    <li class="breadcrumb-item"><a href="SubLocationsView">Підлокації</a></li>
                    <li class="breadcrumb-item active" aria-current="AddSubLocation">Додати локацію</li>
                  </ol>
                </nav>
       <br />

        <div class="col-12">

    <form id="form1" runat="server">
        <div class="form-group col-6">
             <% if (Context.GetOwinContext().Authentication.User.Identity.IsAuthenticated == true)
                { %>
                <h3>Додати нову локацію</h3><br />
                        <asp:Image ID="Image1" runat="server" Width="200px" ImageAlign="Middle" ImageUrl="~/Img/Heros/default.jpg" />
                <br />
                <br />
                Ім'я локації
            <asp:TextBox ID="TextBox1" runat="server" class="form-control" ></asp:TextBox>
            <br />
            Опис локації
            <asp:Textbox ID="TextBox2" runat="server" class="form-control"></asp:Textbox>
            <br />
            Мапа локації
            <asp:DropDownList AutoPostBack="true" ID="TextBox4" runat="server" class="form-select" DataSourceID="SqlDataSource1" DataTextField="idLocationPicture" DataValueField="idLocationPicture" OnSelectedIndexChanged="TextBox4_SelectedIndexChanged" AppendDataBoundItems="true">
                             <asp:ListItem Text="Немає" Value="" />
            </asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DSTConnectionString %>" SelectCommand="SELECT [idLocationPicture], [locationPictureData] FROM [LocationPicture]"></asp:SqlDataSource>
            <br />
   
            <asp:Button ID="Button1" class="btn btn-dark" runat="server" Text="Додати" OnClick="Button1_Click" />
        <%} %>
            <% else { %>
             <h3 class="alert alert-danger">Недостатньо прав</h3>
            <%} %>
        </div>
        <br /><br />
            
    </form>
            </div></div>
</body>
</html>
