﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Pages_HerosView : System.Web.UI.Page
{
    System.Data.SqlClient.SqlConnection sqlConnection = new System.Data.SqlClient.SqlConnection(@"Data Source = FRANC; Initial Catalog = DST; Integrated Security = True");

    protected void Page_Load(object sender, EventArgs e)
    {
        SqlDataSource1.DataBind();
        GridView1.DataBind();
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Web.UI.HtmlControls.HtmlImage imageControl = (System.Web.UI.HtmlControls.HtmlImage)e.Row.FindControl("imageControl");
            if (((DataRowView)e.Row.DataItem)["heroPictureData"] != DBNull.Value)
            {
                imageControl.Src = "data:image/png;base64," + Convert.ToBase64String((byte[])(((DataRowView)e.Row.DataItem))["heroPictureData"]);
            }
        }
    }

    protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var txt = GridView1.Rows[e.NewEditIndex].Cells[0].Text;

        Response.Redirect("EditHero.aspx?id=" + txt.ToString());
    }

    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
        cmd.CommandType = System.Data.CommandType.Text;
        var txt = GridView1.Rows[e.RowIndex].Cells[0].Text;
        cmd.CommandText = "DELETE FROM [Heros] WHERE idHero = @idHero";
        cmd.Connection = sqlConnection;
        sqlConnection.Open();
        SqlParameter idQuest = new SqlParameter("@idHero", txt.ToString());
        cmd.Parameters.Add(idQuest);
        if(cmd.ExecuteNonQuery() > 0)
            MessageBox(this, txt);
        cmd.ExecuteNonQuery();
        sqlConnection.Close();
        SqlDataSource1.DataBind();
        GridView1.DataBind();
    }

    public static void MessageBox(System.Web.UI.Page page, string strMsg)
    {
        ScriptManager.RegisterClientScriptBlock(page, page.GetType(), "alertMessage", "alert('" + strMsg + "')", true);
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        SqlDataSource1.DataBind();
        GridView1.DataBind();
    }
}