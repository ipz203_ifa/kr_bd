﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class EditSubLocation : System.Web.UI.Page
{
    System.Data.SqlClient.SqlConnection sqlConnection = new System.Data.SqlClient.SqlConnection(@"Data Source = FRANC; Initial Catalog = DST; Integrated Security = True");

    protected void Page_Load(object sender, EventArgs e)
    {

    }
    void GetCurrentValues()
    {
        System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
        cmd.CommandText = "SELECT * FROM [SubLocations] WHERE idSubLocation = @idSubLocation";
        cmd.Connection = sqlConnection;
        sqlConnection.Open();
        SqlParameter idQuest = new SqlParameter("@idSubLocation", Request.QueryString["id"].ToString());
        cmd.Parameters.Add(idQuest);
        SqlDataReader dr = cmd.ExecuteReader();
        if (dr.Read() == true)
        {
            TextBox1.SelectedValue = dr.GetSqlValue(1).ToString();
            TextBox2.Text = dr.GetSqlValue(2).ToString();
            TextBox4.Text = dr.GetSqlValue(3).ToString();
        }
        sqlConnection.Close();
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
        cmd.CommandType = System.Data.CommandType.Text;
        cmd.CommandText = "UPDATE [SubLocations] SET nameSubLocation = @nameSubLocation, idLocation = @idLocation, descSubLocation = @descSubLocation WHERE idSubLocation = @idSubLocation";
        cmd.Connection = sqlConnection;
        sqlConnection.Open();
        SqlParameter nameSubLocation = new SqlParameter("@nameSubLocation", TextBox2.Text);
        SqlParameter idLocation = new SqlParameter("@idLocation", TextBox1.SelectedItem.Value);
        SqlParameter descSubLocation = new SqlParameter("@descSubLocation", TextBox4.Text);
        SqlParameter idSubLocation = new SqlParameter("@idSubLocation", Request.QueryString["id"]);
        cmd.Parameters.Add(nameSubLocation);
        cmd.Parameters.Add(idLocation);
        cmd.Parameters.Add(descSubLocation);
        cmd.Parameters.Add(idSubLocation);
        cmd.ExecuteNonQuery();
        sqlConnection.Close();
        Response.Redirect("SubLocationsView.aspx");
    }

    protected void SqlDataSource1_Init(object sender, EventArgs e)
    {
        GetCurrentValues();
    }
}