﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Other.aspx.cs" Inherits="Other" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Index</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous" />
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
</head>
<body>
        <div class="container">
            <div class="col-12">
    <form id="form1" runat="server">
    <nav class="navbar navbar-dark bg-dark justify-content-between">
      <a class="navbar-brand mx-5" style="color: white; font-size: xx-large; font-weight: bold" href="Index">Don't Starve Together</a>
    </nav>
        <br />
       <nav aria-label="breadcrumb">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="Index">Головна</a></li>
                    <li class="breadcrumb-item active" aria-current="Other">Авторизація</li>
                  </ol>
                </nav>
       <br />
        <div class="col-12">
                    <h1>Авторизація</h1><br /><br />
            
                            <asp:LoginView runat="server" ViewStateMode="Disabled">
                <AnonymousTemplate>
                     <asp:Button ID="Button2" class="btn btn-dark" runat="server" Text="Реєстрація" OnClick="Button2_Click" />
            <asp:Button ID="Button3" class="btn btn-dark" runat="server" Text="Вхід" OnClick="Button3_Click" />              
            
                </AnonymousTemplate>
                        <LoggedInTemplate>
                                <a runat="server" href="~/Account/Manage" class="alert alert-secondary h5 " title="Manage your account">Добрий день, <%: Context.User.Identity.GetUserName()  %>!</a><br />
                                    <br /><asp:LoginStatus runat="server" class="btn btn-dark" LogoutText="Вийти" OnLoggingOut="Unnamed_LoggingOut" />
                        </LoggedInTemplate>
                    </asp:LoginView><br /><br />
            <% if (Context.GetOwinContext().Authentication.User.IsInRole("admin") == true)
                { %>
            <asp:Label ID="Label2" runat="server" Text="Доступні дії: "></asp:Label><br /><br />
        <asp:Button ID="Button1" class="btn btn-dark" runat="server" Text="Створити бекап БД" OnClick="Button1_Click" />
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DSTConnectionString %>" SelectCommand="DB_Backup_proc" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
                            <br />
        <asp:Label ID="Label3" class="form-control" runat="server"></asp:Label><br />
            <%} %>
                    <br /><br />
                    <br /><br />
        <br />

    </form>
            </div>
    </div>
</body>
</html>
