﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class EditQuest : System.Web.UI.Page
{
    System.Data.SqlClient.SqlConnection sqlConnection = new System.Data.SqlClient.SqlConnection(@"Data Source = FRANC; Initial Catalog = DST; Integrated Security = True");
    
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
        cmd.CommandText = "UPDATE [Quests] SET nameQuest = @nameQuest, idSubLocation = @idSubLocation, " +
                   "idHero = @idHero, conditionQuest = @conditionQuest, typeQuest = @typeQuest, rewardExpQuest = @rewardExpQuest, " +
                   "rewardMoneyQuest = @rewardMoneyQuest, recommendedLevelQuest = @recommendedLevelQuest, goalsQuest = @goalsQuest WHERE idQuest = @idQuest";
        cmd.Connection = sqlConnection;
        sqlConnection.Open();
        SqlParameter nameQuest = new SqlParameter("@nameQuest", TextBox1.Text);
        SqlParameter idSubLocation = new SqlParameter("@idSubLocation", TextBox2.SelectedItem.Value);
        SqlParameter idHero = new SqlParameter("@idHero", TextBox4.SelectedItem.Value);
        SqlParameter conditionQuest = new SqlParameter("@conditionQuest", TextBox5.Text);
        SqlParameter typeQuest = new SqlParameter("@typeQuest", TextBox6.SelectedItem.Value);
        SqlParameter rewardExpQuest = new SqlParameter("@rewardExpQuest", TextBox7.Text);
        SqlParameter rewardMoneyQuest = new SqlParameter("@rewardMoneyQuest", TextBox8.Text);
        SqlParameter recommendedLevelQuest = new SqlParameter("@recommendedLevelQuest", TextBox9.Text);
        SqlParameter goalsQuest = new SqlParameter("@goalsQuest", TextBox10.Text);
        SqlParameter idQuest = new SqlParameter("@idQuest", Request.QueryString["id"]);
        cmd.Parameters.Add(nameQuest);
        cmd.Parameters.Add(idSubLocation);
        cmd.Parameters.Add(idHero);
        cmd.Parameters.Add(conditionQuest);
        cmd.Parameters.Add(typeQuest);
        cmd.Parameters.Add(rewardExpQuest);
        cmd.Parameters.Add(rewardMoneyQuest);
        cmd.Parameters.Add(recommendedLevelQuest);
        cmd.Parameters.Add(goalsQuest);
        cmd.Parameters.Add(idQuest);
        if (cmd.ExecuteNonQuery() == 1) 
        {
            MessageBox(this, "You have changed quest with id " + Request.QueryString["id"].ToString() + "!");
            Response.Redirect("QuestsView.aspx");
        }
        cmd.ExecuteNonQuery();
        sqlConnection.Close();
    }

    public static void MessageBox(System.Web.UI.Page page, string strMsg)
    {
        ScriptManager.RegisterClientScriptBlock(page, page.GetType(), "alertMessage", "alert('" + strMsg + "')", true);
    }

    void GetCurrentValues()
    {
        System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
        cmd.CommandText = "SELECT * FROM [Quests] WHERE idQuest = @idQuest";
        cmd.Connection = sqlConnection;
        sqlConnection.Open();
        SqlParameter idQuest = new SqlParameter("@idQuest", Request.QueryString["id"].ToString());
        cmd.Parameters.Add(idQuest);
        SqlDataReader dr = cmd.ExecuteReader();
        if (dr.Read() == true)
        {
            TextBox1.Text = dr.GetSqlValue(1).ToString();
            TextBox2.SelectedValue = dr.GetSqlValue(2).ToString();
            TextBox4.SelectedValue = dr.GetSqlValue(3).ToString();
            TextBox5.Text = dr.GetSqlValue(4).ToString();
            TextBox6.SelectedValue = dr.GetSqlValue(5).ToString();
            TextBox7.Text = dr.GetSqlValue(6).ToString();
            TextBox8.Text = dr.GetSqlValue(7).ToString();
            TextBox9.Text = dr.GetSqlValue(8).ToString();
            TextBox10.Text = dr.GetSqlValue(9).ToString();
        }
        sqlConnection.Close();
    }

    protected void SqlDataSource1_Init(object sender, EventArgs e)
    {
        GetCurrentValues();
    }
}