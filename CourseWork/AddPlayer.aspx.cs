﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AddPlayer : System.Web.UI.Page
{
    System.Data.SqlClient.SqlConnection sqlConnection = new System.Data.SqlClient.SqlConnection(@"Data Source = FRANC; Initial Catalog = DST; Integrated Security = True");

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
        cmd.CommandType = System.Data.CommandType.Text;
        cmd.CommandText = "INSERT [Player] (namePlayer, idPlayerItem, hpPlayer, levelPlayer, damagePlayer, idSubLocation, idHero, idQuest) " +
            "VALUES(@namePlayer, @idPlayerItem, @hpPlayer, @levelPlayer, @damagePlayer, @idSubLocation, @idHero, @idQuest)";
        cmd.Connection = sqlConnection;
        sqlConnection.Open();
        var hp = Int32.Parse(TextBox2.Text) * 5;
        var dam = Int32.Parse(TextBox2.Text) * 2;
        SqlParameter namePlayer = new SqlParameter("@namePlayer", TextBox1.Text);
        SqlParameter idPlayerItem = new SqlParameter("@idPlayerItem", '0');
        SqlParameter hpPlayer = new SqlParameter("@hpPlayer", hp);
        SqlParameter levelPlayer = new SqlParameter("@levelPlayer", TextBox2.Text);
        SqlParameter damagePlayer = new SqlParameter("@damagePlayer", dam);
        SqlParameter idSubLocation = new SqlParameter("@idSubLocation", TextBox4.SelectedItem.Value);
        SqlParameter idHero = new SqlParameter("@idHero", TextBox5.SelectedItem.Value);
        SqlParameter idQuest = new SqlParameter("@idQuest", TextBox6.SelectedItem.Value);
        cmd.Parameters.Add(namePlayer);
        cmd.Parameters.Add(idPlayerItem);
        cmd.Parameters.Add(hpPlayer);
        cmd.Parameters.Add(levelPlayer);
        cmd.Parameters.Add(damagePlayer);
        cmd.Parameters.Add(idSubLocation);
        cmd.Parameters.Add(idHero);
        cmd.Parameters.Add(idQuest);
        cmd.ExecuteNonQuery();
        sqlConnection.Close();
        Response.Redirect("PlayerView.aspx");
    }
}