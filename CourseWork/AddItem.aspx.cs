﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AddItem : System.Web.UI.Page
{
    System.Data.SqlClient.SqlConnection sqlConnection = new System.Data.SqlClient.SqlConnection(@"Data Source = FRANC; Initial Catalog = DST; Integrated Security = True");

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
        cmd.CommandType = System.Data.CommandType.Text;
        cmd.CommandText = "INSERT [Items]  (nameItem, typeItem, idLocation, descItem, weightItem, costItem) " +
            "VALUES(@nameItem, @typeItem, @idLocation, @descItem, @weightItem, @costItem)";
        cmd.Connection = sqlConnection;
        sqlConnection.Open();
        SqlParameter nameItem = new SqlParameter("@nameItem", TextBox1.Text);
        SqlParameter typeItem = new SqlParameter("@typeItem", TextBox2.SelectedItem.Value);
        SqlParameter idLocation = new SqlParameter("@idLocation", TextBox4.SelectedItem.Value);
        SqlParameter descItem = new SqlParameter("@descItem", TextBox5.Text);
        SqlParameter weightItem = new SqlParameter("@weightItem", TextBox6.Text);
        SqlParameter costItem = new SqlParameter("@costItem", TextBox7.Text);
        cmd.Parameters.Add(nameItem);
        cmd.Parameters.Add(typeItem);
        cmd.Parameters.Add(idLocation);
        cmd.Parameters.Add(descItem);
        cmd.Parameters.Add(weightItem);
        cmd.Parameters.Add(costItem);
        cmd.ExecuteNonQuery();
        sqlConnection.Close();
        Response.Redirect("ItemsView.aspx");
    }
}