﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="StatPlayer.aspx.cs" Inherits="StatPlayer" %>

<%@ Register assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" namespace="System.Web.UI.DataVisualization.Charting" tagprefix="asp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>StatPlayer</title>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous" />
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
</head>
<body>
     <div class="container">
    <nav class="navbar navbar-dark bg-dark justify-content-between">
      <a class="navbar-brand mx-5" style="color: white; font-size: xx-large; font-weight: bold" href="Index">Don't Starve Together</a>
    </nav>
       <br />
       <nav aria-label="breadcrumb">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="Index">Головна</a></li>
                    <li class="breadcrumb-item"><a href="PlayerView">Гравці</a></li>
                    <li class="breadcrumb-item active" aria-current="StatPlayer">Статистика гравця</li>
                  </ol>
                </nav>
       <br />

        <div class="col-12">

    <form id="form1" runat="server">
        <div class="form-group col-6">
                                                                        <% if (Context.GetOwinContext().Authentication.User.Identity.IsAuthenticated == true) { %>
                            <h3>Статистика гравця</h3>
                <br />
            <asp:DropDownList ID="DropDownList1" AutoPostBack="true" class="form-select" runat="server" DataSourceID="SqlDataSource2" DataTextField="namePlayer" DataValueField="idPlayer" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged"></asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:DSTConnectionString %>" SelectCommand="SELECT [idPlayer], [namePlayer] FROM [Player]"></asp:SqlDataSource>
                <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:DSTConnectionString %>" SelectCommand="PlayerHoursBySession" SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DropDownList1" Name="idPlayerForSessionHours" PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
              
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DSTConnectionString %>" SelectCommand="TotalPlayerGameHours" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:ControlParameter ControlID="DropDownList1" Name="idPlayerForTotalGameHours" PropertyName="SelectedValue" Type="Int32" DefaultValue="1" />
            </SelectParameters>
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:DSTConnectionString %>" SelectCommand="QuestsProgress" SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DropDownList1" Name="idPlayerForQuestsProgress" PropertyName="SelectedValue" Type="Int32" DefaultValue="1" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <br />
             <asp:GridView ID="GridView1" runat="server" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="10" DataSourceID="SqlDataSource1" ForeColor="Black" GridLines="Horizontal" AutoGenerateColumns="False" ShowFooter="True" ShowHeaderWhenEmpty="True" HorizontalAlign="Left" Width="300px" EmptyDataText="0">
                    <Columns>
                        <asp:BoundField DataField="playedHours" HeaderText="Всього ігрових годин" ReadOnly="True" SortExpression="playedHours">
                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:BoundField>
                    </Columns>
                <FooterStyle BackColor="#333333" ForeColor="Black" Height="5px" />
                <HeaderStyle BackColor="#28242c" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="White" ForeColor="Black" Font-Size="X-Large" HorizontalAlign="Center" />
                <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#E5E5E5" />
                <SortedAscendingHeaderStyle BackColor="#333333" />
                <SortedDescendingCellStyle BackColor="#E5E5E5" />
                <SortedDescendingHeaderStyle BackColor="#333333" />
        </asp:GridView><br /><br /><br /><br /><br /><br /><br />
                <asp:Chart ID="Chart1" runat="server" DataSourceID="SqlDataSource4" Palette="Excel" Width="1000px"><series><asp:Series Name="Series1" XValueMember="Column1" YValueMembers="layedHours" IsValueShownAsLabel="True"></asp:Series></series><chartareas><asp:ChartArea Name="ChartArea1"></asp:ChartArea></chartareas></asp:Chart>
                <br />
                <br />
                <br />
                <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="10" DataSourceID="SqlDataSource3" ForeColor="Black" GridLines="Horizontal" ShowFooter="True" ShowHeaderWhenEmpty="True" HorizontalAlign="Left" Width="300px" EmptyDataText="0">
                    <Columns>
                        <asp:BoundField DataField="QuestsProgress" HeaderText="Пройдено квестів, %" ReadOnly="True" SortExpression="QuestsProgress">
                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:BoundField>
                    </Columns>
                <FooterStyle BackColor="#333333" ForeColor="Black" Height="5px" />
                <HeaderStyle BackColor="#28242c" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="White" ForeColor="Black" Font-Size="X-Large" HorizontalAlign="Center" />
                <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#E5E5E5" />
                <SortedAscendingHeaderStyle BackColor="#333333" />
                <SortedDescendingCellStyle BackColor="#E5E5E5" />
                <SortedDescendingHeaderStyle BackColor="#333333" />
                </asp:GridView>
                <br />
                <asp:Chart ID="Chart2" runat="server" Palette="Excel" Width="1000px">
                    <Series>
                        <asp:Series ChartType="Pie" IsValueShownAsLabel="True" Name="Series1">
                        </asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea Name="ChartArea1">
                        </asp:ChartArea>
                    </ChartAreas>
                </asp:Chart><br />
            <asp:Chart ID="Chart4" runat="server" Palette="Excel" Width="1000px" DataSourceID="SqlDataSource7">
                    <Series>
                        <asp:Series ChartType="SplineRange" IsValueShownAsLabel="True" Name="Series1" XValueMember="nameQuest" YValueMembers="idQuest" YValuesPerPoint="4">
                        </asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea Name="ChartArea1">
                        </asp:ChartArea>
                    </ChartAreas>
                </asp:Chart>
                <asp:SqlDataSource ID="SqlDataSource7" runat="server" ConnectionString="<%$ ConnectionStrings:DSTConnectionString %>" SelectCommand="QuestsProgressByQuest" SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DropDownList1" Name="idPlayerForQuestsProgressByQuest" PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </div>

        <br />
        <asp:GridView ID="GridView3" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="10" DataSourceID="SqlDataSource5" ForeColor="Black" GridLines="Horizontal" HorizontalAlign="Left" ShowFooter="True" ShowHeaderWhenEmpty="True" Width="300px">
            <Columns>
                <asp:BoundField DataField="TotalItems" HeaderText="Загальна кількість предметів" ReadOnly="True" SortExpression="TotalItems">
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                </asp:BoundField>
            </Columns>
                <FooterStyle BackColor="#333333" ForeColor="Black" Height="5px" />
                <HeaderStyle BackColor="#28242c" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="White" ForeColor="Black" Font-Size="X-Large" HorizontalAlign="Center" />
                <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#E5E5E5" />
                <SortedAscendingHeaderStyle BackColor="#333333" />
                <SortedDescendingCellStyle BackColor="#E5E5E5" />
                <SortedDescendingHeaderStyle BackColor="#333333" />
        </asp:GridView><br /><br /><br /><br /><br /><br /><br /><br /> 
        <asp:SqlDataSource ID="SqlDataSource5" runat="server" ConnectionString="<%$ ConnectionStrings:DSTConnectionString %>" SelectCommand="TotalCountItemsForPlayer" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:ControlParameter ControlID="DropDownList1" Name="idPlayerForCountItems" PropertyName="SelectedValue" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:Chart ID="Chart3" runat="server" Palette="Excel" Width="1000px" DataSourceID="SqlDataSource6">
                    <Series>
                        <asp:Series ChartType="Bar" IsValueShownAsLabel="True" Name="Series1" XValueMember="nameItem" YValueMembers="countPlayerItem">
                        </asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea Name="ChartArea1">
                        </asp:ChartArea>
                    </ChartAreas>
                </asp:Chart>
        <asp:SqlDataSource ID="SqlDataSource6" runat="server" ConnectionString="<%$ ConnectionStrings:DSTConnectionString %>" SelectCommand="CountItemsForPlayerByTypeItem" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:ControlParameter ControlID="DropDownList1" Name="idPlayerForCountItems" PropertyName="SelectedValue" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
        <br /> <%} %>
            <% else { %>
             <h3 class="alert alert-danger">Недостатньо прав</h3>
            <%} %><br /> 
        <br />
    </form>
            </div></div>
    </body>
</html>

