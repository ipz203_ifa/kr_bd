﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data.SqlClient;

public partial class AddPictureLocation : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public static void MessageBox(System.Web.UI.Page page, string strMsg)
    {
        ScriptManager.RegisterClientScriptBlock(page, page.GetType(), "alertMessage", "alert('" + strMsg + "')", true);
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        string strFileName;
        string strFilePath;
        string strFolder;
        strFolder = Server.MapPath("./Img/Locations/");
        strFileName = oFile.PostedFile.FileName;
        strFileName = Path.GetFileName(strFileName);
        if (oFile.Value != "")
        {
            if (!Directory.Exists(strFolder))
            {
                Directory.CreateDirectory(strFolder);
            }
            strFilePath = strFolder + strFileName;
            if (File.Exists(strFilePath))
            {
                lblUploadResult.Text = strFileName + " already exists on the server!";
            }
            else
            {
                oFile.PostedFile.SaveAs(strFilePath);
                if (SavePictureToDB(strFileName))
                    lblUploadResult.Text = strFileName + " has been successfully uploaded.";
            }
        }
        else
        {
            lblUploadResult.Text = "Click 'Browse' to select the file to upload.";
        }
        frmConfirmation.Visible = true;
    }

    bool SavePictureToDB(string strFileName)
    {
        System.Data.SqlClient.SqlConnection sqlConnection = new System.Data.SqlClient.SqlConnection(@"Data Source = FRANC; Initial Catalog = DST; Integrated Security = True");
        System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
        cmd.CommandType = System.Data.CommandType.Text;
        cmd.CommandText = "exec dbo.LocationImportImage @param_name,@param_path, @param_file ";
        cmd.Connection = sqlConnection;
        sqlConnection.Open();
        string strFilePath = "C:\\Users\\pc\\Desktop\\CourseWork\\CourseWork\\Img\\Locations";
        SqlParameter param_name = new SqlParameter("@param_name", TextBox1.Text);
        SqlParameter param_path = new SqlParameter("@param_path", strFilePath);
        SqlParameter param_file = new SqlParameter("@param_file", strFileName);

        cmd.Parameters.Add(param_name);
        cmd.Parameters.Add(param_path);
        cmd.Parameters.Add(param_file);

        if (cmd.ExecuteNonQuery() > 0)
        {
            sqlConnection.Close();
            return true;
        }
        else
        {
            sqlConnection.Close();
            return false;
        }
    }
}