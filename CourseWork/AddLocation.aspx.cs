﻿using System;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using System.Runtime.Remoting.Metadata.W3cXsd2001;

public partial class AddLocation : System.Web.UI.Page
{
    System.Data.SqlClient.SqlConnection sqlConnection = new System.Data.SqlClient.SqlConnection(@"Data Source = FRANC; Initial Catalog = DST; Integrated Security = True");

    protected void Page_Load(object sender, EventArgs e)
    {
        if (TextBox4.SelectedValue == "")
        {
            Image1.ImageUrl = "~/Img/Heros/default.jpg";
        }
    }

    public byte[] ToBytes(string hex)
    {
        var shb = SoapHexBinary.Parse(hex.Remove(hex.Length - 1));

        return shb.Value;
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
        cmd.CommandType = System.Data.CommandType.Text;
        cmd.CommandText = "INSERT [Locations] (nameLocation, descLocation, idLocationPicture) " +
            "VALUES(@nameLocation, @descLocation, @idLocationPicture)";
        cmd.Connection = sqlConnection;
        sqlConnection.Open();
        var pic = TextBox4.SelectedItem.Value;
        if (pic == "") pic = "9";
        SqlParameter idLocationPicture = new SqlParameter("@idLocationPicture", pic);
        SqlParameter nameLocation = new SqlParameter("@nameLocation", TextBox1.Text);
        SqlParameter descLocation = new SqlParameter("@descLocation", TextBox2.Text);
        cmd.Parameters.Add(idLocationPicture);
        cmd.Parameters.Add(nameLocation);
        cmd.Parameters.Add(descLocation);

        if (cmd.ExecuteNonQuery() > 0)
            Response.Redirect("SubLocationsView.aspx");
        cmd.ExecuteNonQuery();
        sqlConnection.Close();
    }

    protected void TextBox4_SelectedIndexChanged(object sender, EventArgs e)
    {
        System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
        cmd.CommandType = System.Data.CommandType.Text;
        cmd.CommandText = "SELECT [locationPictureData] FROM [LocationPicture] WHERE idLocationPicture = @idLocationPicture";
        cmd.Connection = sqlConnection;
        sqlConnection.Open();
        SqlParameter idLocationPicture = new SqlParameter("@idLocationPicture", TextBox4.SelectedItem.Value);
        cmd.Parameters.Add(idLocationPicture);
        byte[] yourImageByteArray;
        SqlDataReader dr = cmd.ExecuteReader();
        if (dr.Read() == true)
        {
            yourImageByteArray = dr.GetFieldValue<byte[]>(0);
            Image1.ImageUrl = "data:image/jpg;base64," + Convert.ToBase64String(yourImageByteArray);
        }
        sqlConnection.Close();
    }
}