﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Pages_SubLocationsView : System.Web.UI.Page
{
    System.Data.SqlClient.SqlConnection sqlConnection = new System.Data.SqlClient.SqlConnection(@"Data Source = FRANC; Initial Catalog = DST; Integrated Security = True");

    protected void Page_Load(object sender, EventArgs e)
    {
        SqlDataSource1.DataBind();
        GridView1.DataBind();
    }

    public static void MessageBox(System.Web.UI.Page page, string strMsg)
    {
        ScriptManager.RegisterClientScriptBlock(page, page.GetType(), "alertMessage", "alert('" + strMsg + "')", true);
    }

    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
        cmd.CommandType = System.Data.CommandType.Text;
        var txt = GridView1.Rows[e.RowIndex].Cells[0].Text;
        cmd.CommandText = "DELETE FROM [SubLocations] WHERE idSubLocation = @idSubLocation";
        cmd.Connection = sqlConnection;
        sqlConnection.Open();
        SqlParameter idSubLocation = new SqlParameter("@idSubLocation", txt.ToString());
        cmd.Parameters.Add(idSubLocation);
        cmd.ExecuteNonQuery();
        sqlConnection.Close();
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Web.UI.HtmlControls.HtmlImage imageControl = (System.Web.UI.HtmlControls.HtmlImage)e.Row.FindControl("imageControl");
            if (((DataRowView)e.Row.DataItem)["locationPictureData"] != DBNull.Value)
            {
                imageControl.Src = "data:image/png;base64," + Convert.ToBase64String((byte[])(((DataRowView)e.Row.DataItem))["locationPictureData"]);
            }
        }
    }

    protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var txt = GridView1.Rows[e.NewEditIndex].Cells[0].Text;
        //MessageBox(this, txt);
        Response.Redirect("EditSubLocation.aspx?id=" + txt.ToString());
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        //if (TextBox1.Text != "")
        //{
        //    DropDownList1.SelectedIndex = 0;
        //    DropDownList1.Enabled = false;
        //}
        //else
        //{
        //    TextBox1.Text = "";
        //    TextBox1.Enabled = false;
        //    DropDownList1.Enabled = true;
        //}

        SqlDataSource1.DataBind();
        GridView1.DataBind();
        //Response.Redirect("SubLocationsView.aspx");
    }

    protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        //if (DropDownList1.SelectedIndex != 0)
        //{
        //    TextBox1.Text = "";
        //    TextBox1.Enabled = false;
        //}
        //else
        //{
        //    DropDownList1.SelectedIndex = 0;
        //    DropDownList1.Enabled = false;
        //    TextBox1.Enabled = true;
        //}

        //Response.Redirect("SubLocationsView.aspx");
    }
}