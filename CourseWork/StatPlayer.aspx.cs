﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class StatPlayer : System.Web.UI.Page
{
    System.Data.SqlClient.SqlConnection sqlConnection = new System.Data.SqlClient.SqlConnection(@"Data Source = FRANC; Initial Catalog = DST; Integrated Security = True");

    protected void Page_Load(object sender, EventArgs e)
    {
        SqlDataSource1.DataBind();
        SqlDataSource3.DataBind();
        SqlDataSource2.DataBind();
        SqlDataSource4.DataBind();
        GridView1.DataBind();
        SqlDataSource3.DataBind();
        GridView2.DataBind();

        System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
        cmd.CommandType = System.Data.CommandType.Text;
        cmd.CommandText = "exec QuestsProgress @idPlayer";
        cmd.Connection = sqlConnection;
        sqlConnection.Open();
        SqlParameter idPlayer = new SqlParameter("@idPlayer", DropDownList1.SelectedValue);
        cmd.Parameters.Add(idPlayer);
        SqlDataReader reader = cmd.ExecuteReader();
        while (reader.Read())
        {
            Chart2.Series[0].Points.AddY((int)reader[0]);
            Chart2.Series[0].Points.AddY(100 - (int)reader[0]);
        }
        sqlConnection.Close();
    }

    protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        SqlDataSource1.DataBind();
        SqlDataSource3.DataBind();
        SqlDataSource2.DataBind();
        SqlDataSource4.DataBind();
        GridView1.DataBind();
        SqlDataSource3.DataBind();
        GridView2.DataBind();
    }
}