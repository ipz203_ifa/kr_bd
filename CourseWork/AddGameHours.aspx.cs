﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AddGameHours : System.Web.UI.Page
{
    System.Data.SqlClient.SqlConnection sqlConnection = new System.Data.SqlClient.SqlConnection(@"Data Source = FRANC; Initial Catalog = DST; Integrated Security = True");

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
        cmd.CommandType = System.Data.CommandType.Text;
        cmd.CommandText = "INSERT [GameHours] (startTime, endTime, idPlayer) " +
            "VALUES(@startTime, @endTime, @idPlayer)";
        cmd.Connection = sqlConnection;
        sqlConnection.Open();
        SqlParameter startTime = new SqlParameter("@startTime", TextBox2.Text);
        SqlParameter endTime = new SqlParameter("@endTime", TextBox3.Text);
        SqlParameter idPlayer = new SqlParameter("@idPlayer", TextBox1.SelectedValue);
        cmd.Parameters.Add(startTime);
        cmd.Parameters.Add(endTime);
        cmd.Parameters.Add(idPlayer);
        MessageBox(this, TextBox2.Text);
        //cmd.ExecuteNonQuery();
        sqlConnection.Close();
        Response.Redirect("PlayerView.aspx");
    }


    public static void MessageBox(System.Web.UI.Page page, string strMsg)
    {
        ScriptManager.RegisterClientScriptBlock(page, page.GetType(), "alertMessage", "alert('" + strMsg + "')", true);
    }
}