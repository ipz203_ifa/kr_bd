﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AddGameHours.aspx.cs" Inherits="AddGameHours" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>NewPlayer</title>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous" />
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
</head>
<body>
     <div class="container">
    <nav class="navbar navbar-dark bg-dark justify-content-between">
      <a class="navbar-brand mx-5" style="color: white; font-size: xx-large; font-weight: bold" href="Index">Don't Starve Together</a>
    </nav>
       <br />
       <nav aria-label="breadcrumb">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="Index">Головна</a></li>
                    <li class="breadcrumb-item"><a href="PlayerView">Гравці</a></li>
                    <li class="breadcrumb-item active" aria-current="AddPlayer">Додати ігрові години гравцю</li>
                  </ol>
                </nav>
       <br />

        <div class="col-12">

    <form id="form1" runat="server">
        <div class="form-group col-6">
             <% if (Context.GetOwinContext().Authentication.User.Identity.IsAuthenticated == true)
                { %>
                <h3>Додати ігрові години гравцю</h3>
                <br />
            Гравець
            <asp:DropDownList ID="TextBox1" class="form-select" runat="server" DataSourceID="SqlDataSource1" DataTextField="namePlayer" DataValueField="idPlayer"></asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DSTConnectionString %>" SelectCommand="SELECT [namePlayer], [idPlayer] FROM [Player]"></asp:SqlDataSource>
            <br />
            Початок ігрового сеансу
            <asp:TextBox textmode="DateTimeLocal" type="datetime" id ="TextBox2" runat="server" name="TextBox2" class="form-control" />
             <br />
            Кінець ігрового сеансу
            <asp:TextBox textmode="DateTimeLocal" type="datetime" id ="TextBox3" runat="server" name="TextBox3" class="form-control" /><br />
            <br />
            <br />
            <asp:Button ID="Button1" class="btn btn-dark" runat="server" OnClick="Button1_Click" Text="Додати" />
                        <%} %>
            <% else { %>
             <h3 class="alert alert-danger">Недостатньо прав</h3>
            <%} %>
        </div>
                <br /><br />
    </form>
            </div>
    </body>
</html>
