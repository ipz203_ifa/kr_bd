﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="HerosView.aspx.cs" Inherits="Pages_HerosView" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Heros</title>
 <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous" />
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
</head>
<body>
    <div class="container">
    <nav class="navbar navbar-dark bg-dark justify-content-between">
      <a class="navbar-brand mx-5" style="color: white; font-size: xx-large; font-weight: bold" href="Index">Don't Starve Together</a>
    </nav>
        <br />
       <nav aria-label="breadcrumb">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="Index">Головна</a></li>
                    <li class="breadcrumb-item active" aria-current="Heros">Персонажі</li>
                  </ol>
                </nav>
       <br />
        <div class="col-12">
    <form id="form1" runat="server">
    <h1>Персонажі</h1><br />

         <div class="input-group">
  <div class="form-outline">
    <asp:Textbox type="search" placeholder="Search" id="TextBox1" runat="server" class="form-control" Text="" />
  </div>
                 <asp:Button ID="Button1" class="btn btn-dark" runat="server" Text="Пошук" OnClick="Button1_Click" />
</div><br />
                        <asp:LoginView runat="server" ViewStateMode="Disabled"><LoggedInTemplate>
         <asp:Label ID="Label2" runat="server" Text="Доступні дії: "></asp:Label><br />
     <a href="AddHero.aspx" class="btn btn-dark">Додати нового персонажа</a>
     <a href="AddPictureHero.aspx" class="btn btn-dark">Додати нове фото персонажа</a><br /><br />
           </LoggedInTemplate></asp:LoginView>

        <asp:Label ID="Label1" runat="server" Text="Оберіть локацію: "></asp:Label>
            <asp:DropDownList ID="DropDownList1" class="form-select" AutoPostBack="true" runat="server" DataSourceID="SqlDataSource2" DataTextField="nameLocation" DataValueField="nameLocation" AppendDataBoundItems="true">
            <asp:ListItem Text="Всі" Value="%" />
        </asp:DropDownList>
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:DSTConnectionString %>" SelectCommand="SELECT [idLocation], [nameLocation] FROM [Locations]"></asp:SqlDataSource>
        <br />
        <asp:Label ID="Label3" runat="server" Text="Оберіть тип героя: "></asp:Label>
            <asp:DropDownList ID="DropDownList2" class="form-select" AutoPostBack="True" runat="server" DataSourceID="SqlDataSource3" DataTextField="nameTypeHero" DataValueField="nameTypeHero" AppendDataBoundItems="True">
            <asp:ListItem Text="Всі" Value="%" />
        </asp:DropDownList>
        <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:DSTConnectionString %>" SelectCommand="SELECT * FROM [TypesHero]"></asp:SqlDataSource>
        <br />
        <asp:Label ID="Label4" runat="server" Text="Оберіть клас героя: "></asp:Label>
            <asp:DropDownList ID="DropDownList3" class="form-select" AutoPostBack="True" runat="server" DataSourceID="SqlDataSource4" DataTextField="nameClassHero" DataValueField="nameClassHero" AppendDataBoundItems="True">
            <asp:ListItem Text="Всі" Value="%" />
        </asp:DropDownList>
        <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:DSTConnectionString %>" SelectCommand="SELECT * FROM [ClassesHero]"></asp:SqlDataSource>
        <br />
        <div id="Image1" property="og:image">
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="idHero" DataSourceID="SqlDataSource1" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="10" ForeColor="Black" GridLines="Horizontal" OnRowDataBound="GridView1_RowDataBound" OnRowEditing="GridView1_RowEditing" OnRowDeleting="GridView1_RowDeleting" AllowSorting="True" AllowPaging="True" ShowHeaderWhenEmpty="True" HorizontalAlign="Center">
                <Columns>
                    <asp:BoundField DataField="idHero" HeaderText="№" ReadOnly="True" SortExpression="idHero" />
                    <asp:BoundField DataField="nameHero" HeaderText="Ім'я" SortExpression="nameHero" />
                    <asp:TemplateField HeaderText="Картинка">
                        <ItemTemplate>
                          <img src='<%# Eval("heroPictureData") %>' id="imageControl" runat="server" width="200" />
                        </ItemTemplate>   
                    </asp:TemplateField> 
                    <asp:BoundField DataField="hpHero" HeaderText="Очки здоров'я" SortExpression="hpHero" />
                    <asp:BoundField DataField="expHero" HeaderText="Очки досвіду" SortExpression="expHero" />
                    <asp:BoundField DataField="nameTypeHero" HeaderText="Тип" SortExpression="nameTypeHero" />
                    <asp:BoundField DataField="nameClassHero" HeaderText="Клас" SortExpression="nameClassHero" />
                    <asp:BoundField DataField="nameLocation" HeaderText="Локація" SortExpression="nameLocation" />
                    <asp:BoundField DataField="descHero" HeaderText="Опис" SortExpression="descHero" />
                    <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
                </Columns>
                <FooterStyle BackColor="#333333" ForeColor="Black" />
                <HeaderStyle BackColor="#28242c" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="White" ForeColor="Black" Font-Size="X-Large" HorizontalAlign="Center" />
                <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#E5E5E5" />
                <SortedAscendingHeaderStyle BackColor="#333333" />
                <SortedDescendingCellStyle BackColor="#E5E5E5" />
                <SortedDescendingHeaderStyle BackColor="#333333" />
            </asp:GridView>
            <asp:SqlDataSource ID="SqlDataSource1" CancelSelectOnNullParameter="False" runat="server" ConnectionString="<%$ ConnectionStrings:DSTConnectionString %>" SelectCommand="SELECT * FROM [StructHeros] WHERE (([nameHero] LIKE '%' + @nameHero + '%') AND ([nameLocation] LIKE '%' + @nameLocation + '%') AND ([nameTypeHero] LIKE '%' + @nameTypeHero + '%') AND ([nameClassHero] LIKE '%' + @nameClassHero + '%'))">
                <FilterParameters>
                   </FilterParameters>
                <SelectParameters>
                    <asp:ControlParameter ControlID="TextBox1" DefaultValue="%" Name="nameHero" PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="DropDownList1" DefaultValue="%" Name="nameLocation" PropertyName="SelectedValue" Type="String" />
                    <asp:ControlParameter ControlID="DropDownList2" DefaultValue="%" Name="nameTypeHero" PropertyName="SelectedValue" Type="String" />
                    <asp:ControlParameter ControlID="DropDownList3" DefaultValue="%" Name="nameClassHero" PropertyName="SelectedValue" Type="String" />
                </SelectParameters>
            </asp:SqlDataSource>
        </div><br /><br />
    </form>
            </div>
        </div>
</body>
</html>
