﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Other : System.Web.UI.Page
{
    System.Data.SqlClient.SqlConnection sqlConnection = new System.Data.SqlClient.SqlConnection(@"Data Source = FRANC; Initial Catalog = DST; Integrated Security = True");

    protected void Page_Load(object sender, EventArgs e)
    {
        Label3.Text = "";
        Label3.Visible = false;
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
        cmd.CommandText = "exec [DB_Backup_proc] @pathForBackUP";
        cmd.Connection = sqlConnection;
        sqlConnection.Open();
        SqlParameter pathForBackUP = new SqlParameter("@pathForBackUP", $"C:\\Program Files\\Microsoft SQL Server\\MSSQL15.MSSQLSERVER\\MSSQL\\Backup\\DST_FullDbBkup_{DateTime.Now.Date.ToShortDateString()}.bak");
        cmd.Parameters.Add(pathForBackUP);
        cmd.ExecuteNonQuery();
        Label3.Text = $"DST_FullDbBkup_{DateTime.Now.Date.ToShortDateString()}.bak created!";
        Label3.Visible = true;
        sqlConnection.Close();
    }

    protected void Button2_Click(object sender, EventArgs e)
    {
        Response.Redirect("Account/Register.aspx");
    }

    protected void Button3_Click(object sender, EventArgs e)
    {
        Response.Redirect("Account/Login.aspx");
    }

    protected void Unnamed_LoggingOut(object sender, LoginCancelEventArgs e)
    {
        Context.GetOwinContext().Authentication.SignOut();
    }
}