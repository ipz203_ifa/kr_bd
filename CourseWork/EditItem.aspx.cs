﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class EditItem : System.Web.UI.Page
{
    System.Data.SqlClient.SqlConnection sqlConnection = new System.Data.SqlClient.SqlConnection(@"Data Source = FRANC; Initial Catalog = DST; Integrated Security = True");

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
        cmd.CommandText = "UPDATE [Items] SET nameItem = @nameItem, typeItem = @typeItem, " +
                   "idLocation = @idLocation, descItem = @descItem, weightItem = @weightItem, costItem = @costItem " +
                   "WHERE idItem = @idItem";
        cmd.Connection = sqlConnection;
        sqlConnection.Open();
        SqlParameter nameItem = new SqlParameter("@nameItem", TextBox1.Text);
        SqlParameter typeItem = new SqlParameter("@typeItem", TextBox2.SelectedItem.Value);
        SqlParameter idLocation = new SqlParameter("@idLocation", TextBox4.SelectedItem.Value);
        SqlParameter descItem = new SqlParameter("@descItem", TextBox5.Text);
        SqlParameter weightItem = new SqlParameter("@weightItem", TextBox6.Text);
        SqlParameter costItem = new SqlParameter("@costItem", TextBox7.Text);
        SqlParameter idItem = new SqlParameter("@idItem", Request.QueryString["id"]);
        cmd.Parameters.Add(nameItem);
        cmd.Parameters.Add(typeItem);
        cmd.Parameters.Add(idLocation);
        cmd.Parameters.Add(descItem);
        cmd.Parameters.Add(weightItem);
        cmd.Parameters.Add(costItem);
        cmd.Parameters.Add(idItem);
        if (cmd.ExecuteNonQuery() == 1)
        {
            MessageBox(this, "You have changed item with id " + Request.QueryString["id"].ToString() + "!");
            Response.Redirect("ItemsView.aspx");
        }
        cmd.ExecuteNonQuery();
        sqlConnection.Close();
    }
    public static void MessageBox(System.Web.UI.Page page, string strMsg)
    {
        ScriptManager.RegisterClientScriptBlock(page, page.GetType(), "alertMessage", "alert('" + strMsg + "')", true);
    }

    void GetCurrentValues()
    {
        System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
        cmd.CommandText = "SELECT * FROM [Items] WHERE idItem = @idItem";
        cmd.Connection = sqlConnection;
        sqlConnection.Open();
        SqlParameter idItem = new SqlParameter("@idItem", Request.QueryString["id"].ToString());
        cmd.Parameters.Add(idItem);
        SqlDataReader dr = cmd.ExecuteReader();
        if (dr.Read() == true)
        {
            TextBox1.Text = dr.GetSqlValue(1).ToString();
            TextBox2.SelectedValue = dr.GetSqlValue(2).ToString();
            TextBox4.SelectedValue = dr.GetSqlValue(3).ToString();
            TextBox5.Text = dr.GetSqlValue(4).ToString();
            TextBox6.Text = dr.GetSqlValue(5).ToString();
            TextBox7.Text = dr.GetSqlValue(6).ToString();
        }
        sqlConnection.Close();
    }



    protected void SqlDataSource1_Init(object sender, EventArgs e)
    {
        GetCurrentValues();
    }
}