﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AddPlayerItem : System.Web.UI.Page
{
    System.Data.SqlClient.SqlConnection sqlConnection = new System.Data.SqlClient.SqlConnection(@"Data Source = FRANC; Initial Catalog = DST; Integrated Security = True");

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
        cmd.CommandType = System.Data.CommandType.Text;
        cmd.CommandText = "INSERT [PlayerItems]  (idPlayer, idItem, countPlayerItem) " +
            "VALUES(@idPlayer, @idItem, @countPlayerItem)";
        cmd.Connection = sqlConnection;
        sqlConnection.Open();
        SqlParameter idPlayer = new SqlParameter("@idPlayer", TextBox2.SelectedItem.Value);
        SqlParameter idItem = new SqlParameter("@idItem", TextBox4.SelectedItem.Value);
        SqlParameter countPlayerItem = new SqlParameter("@countPlayerItem", TextBox5.Text);
        cmd.Parameters.Add(idPlayer);
        cmd.Parameters.Add(idItem);
        cmd.Parameters.Add(countPlayerItem);
        cmd.ExecuteNonQuery();
        sqlConnection.Close();
        Response.Redirect("AddPlayerItem.aspx");
    }

    public static void MessageBox(System.Web.UI.Page page, string strMsg)
    {
        ScriptManager.RegisterClientScriptBlock(page, page.GetType(), "alertMessage", "alert('" + strMsg + "')", true);
    }

    protected void TextBox2_SelectedIndexChanged(object sender, EventArgs e)
    {
        SqlDataSource1.DataBind();
        GridView1.DataBind();
    }
}