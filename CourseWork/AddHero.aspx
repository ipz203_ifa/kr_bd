﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AddHero.aspx.cs" Inherits="AddHero" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>NewHero</title>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous" />
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
</head>
<body>
     <div class="container">
    <nav class="navbar navbar-dark bg-dark justify-content-between">
      <a class="navbar-brand mx-5" style="color: white; font-size: xx-large; font-weight: bold" href="Index">Don't Starve Together</a>
    </nav>
       <br />
       <nav aria-label="breadcrumb">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="Index">Головна</a></li>
                    <li class="breadcrumb-item"><a href="HerosView">Персонажі</a></li>
                    <li class="breadcrumb-item active" aria-current="AddSubLocation">Додати нового персонажа</li>
                  </ol>
                </nav>
       <br />

        <div class="col-12">

    <form id="form1" runat="server">
        <div class="form-group col-6">
            <% if (Context.GetOwinContext().Authentication.User.Identity.IsAuthenticated == true)
                { %>
                <h3>Додати нового персонажа</h3>
                <br />
            <asp:Image ID="Image1" runat="server" Width="200px" ImageAlign="Middle" ImageUrl="~/Img/Heros/default.jpg" />
                <br />
                <br />

            Ім'я персонажу
            <asp:TextBox ID="TextBox1" runat="server" required="required" class="form-control"></asp:TextBox>
            <br />
            Очки здоров'я
            <asp:TextBox ID="TextBox2" runat="server" class="form-control"></asp:TextBox>
            <br />
            Очки досвіду
            <asp:TextBox ID="TextBox4" runat="server" class="form-control"></asp:TextBox>
            <br />
            Тип героя
            <asp:DropDownList ID="TextBox5" class="form-select" runat="server" DataSourceID="SqlDataSource1" DataTextField="nameTypeHero" DataValueField="idTypeHero"></asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DSTConnectionString %>" SelectCommand="SELECT * FROM [TypesHero]"></asp:SqlDataSource>
            <br />
            Клас героя
            <asp:DropDownList ID="TextBox6" class="form-select" runat="server" DataSourceID="SqlDataSource2" DataTextField="nameClassHero" DataValueField="idClassHero"></asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:DSTConnectionString %>" SelectCommand="SELECT * FROM [ClassesHero]"></asp:SqlDataSource>
            <br />
            Опис героя
            <asp:TextBox ID="TextBox7" runat="server" class="form-control"></asp:TextBox>
            <br />
            Локація
            <asp:DropDownList ID="TextBox8" class="form-select" runat="server" DataSourceID="SqlDataSource3" DataTextField="nameLocation" DataValueField="idLocation"></asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:DSTConnectionString %>" SelectCommand="SELECT [idLocation], [nameLocation] FROM [Locations]"></asp:SqlDataSource>
            <br />
            Фото героя
            <asp:DropDownList ID="TextBox9" AppendDataBoundItems="true" AutoPostBack="true" class="form-select" runat="server" DataSourceID="SqlDataSource4" DataTextField="idHeroPicture" DataValueField="idHeroPicture" OnSelectedIndexChanged="TextBox9_SelectedIndexChanged">
             <asp:ListItem Text="Немає" Value="" />
            </asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:DSTConnectionString %>" SelectCommand="SELECT [idHeroPicture], [heroPictureData] FROM [HeroPicture]"></asp:SqlDataSource>
            <br />
            <br />

            <asp:Button ID="Button1" class="btn btn-dark" runat="server" OnClick="Button1_Click" Text="Додати" />
       <%} %>
            <% else { %>
             <h3 class="alert alert-danger">Недостатньо прав</h3>
            <%} %>
            </div>
                <br /><br />
    </form>
            </div>
    </body>
</html>
