﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AddPictureHero : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        string strFileName;
        string strFilePath;
        string strFolder;
        strFolder = Server.MapPath("./Img/Heros/");
        // Retrieve the name of the file that is posted.
        strFileName = oFile.PostedFile.FileName;
        strFileName = Path.GetFileName(strFileName);
        if (oFile.Value != "")
        {
            // Create the folder if it does not exist.
            if (!Directory.Exists(strFolder))
            {
                Directory.CreateDirectory(strFolder);
            }
            // Save the uploaded file to the server.
            strFilePath = strFolder + strFileName;
            if (File.Exists(strFilePath))
            {
                lblUploadResult.Text = strFileName + " already exists on the server!";
            }
            else
            {
                oFile.PostedFile.SaveAs(strFilePath);
                if (SavePictureToDB(strFileName))
                    lblUploadResult.Text = strFileName + " has been successfully uploaded.";
            }
        }
        else
        {
            lblUploadResult.Text = "Click 'Browse' to select the file to upload.";
        }
        // Display the result of the upload.
        frmConfirmation.Visible = true;
    }
    bool SavePictureToDB(string strFileName)
    {
        System.Data.SqlClient.SqlConnection sqlConnection = new System.Data.SqlClient.SqlConnection(@"Data Source = FRANC; Initial Catalog = DST; Integrated Security = True");
        System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
        cmd.CommandType = System.Data.CommandType.Text;
        cmd.CommandText = "exec dbo.HeroImportImage @param_name, @param_path, @param_file ";
        cmd.Connection = sqlConnection;
        sqlConnection.Open();
        string strFilePath = "C:\\Users\\franc\\Desktop\\db_coursework-franc\\CourseWork\\Img\\Heros";
        SqlParameter param_name = new SqlParameter("@param_name", TextBox1.Text);
        SqlParameter param_path = new SqlParameter("@param_path", strFilePath);
        SqlParameter param_file = new SqlParameter("@param_file", strFileName);

        cmd.Parameters.Add(param_name);
        cmd.Parameters.Add(param_path);
        cmd.Parameters.Add(param_file);

        if (cmd.ExecuteNonQuery() > 0)
        {
            sqlConnection.Close();
            return true;
        }
        else
        {
            sqlConnection.Close();
            return false;
        }
        //Response.Redirect("HerosView.aspx");
    }
}