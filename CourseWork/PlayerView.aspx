﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PlayerView.aspx.cs" Inherits="Pages_PlayerView" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>Player</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous" />
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
</head>
<body>
    <div class="container">
    <nav class="navbar navbar-dark bg-dark justify-content-between">
      <a class="navbar-brand mx-5" style="color: white; font-size: xx-large; font-weight: bold" href="Index">Don't Starve Together</a>
    </nav>
        <br />
       <nav aria-label="breadcrumb">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="Index">Головна</a></li>
                    <li class="breadcrumb-item active" aria-current="Player">Гравець</li>
                  </ol>
                </nav>
       <br />
        <div class="col-12">
    <form id="form1" runat="server">
    <h1>Інформація про гравця</h1><br />

                 <div class="input-group">
  <div class="form-outline">
    <asp:Textbox type="search" placeholder="Search" id="TextBox1" runat="server" class="form-control" Text="" />
  </div>
                 <asp:Button ID="Button1" class="btn btn-dark" runat="server" Text="Пошук" OnClick="Button1_Click" />
</div><br />
                                <asp:LoginView runat="server" ViewStateMode="Disabled"><LoggedInTemplate>

                     <asp:Label ID="Label2" runat="server" Text="Доступні дії: "></asp:Label><br />
     <a href="AddPlayer.aspx" class="btn btn-dark">Додати нового гравця</a>
     <a href="AddPlayerItem.aspx" class="btn btn-dark">Додати предмет гравцю</a>
     <a href="AddGameHours.aspx" class="btn btn-dark">Додати ігрові години гравцю</a>
     <a href="StatPlayer.aspx" class="btn btn-dark">Переглянути статистику</a><br /><br />
                                    </LoggedInTemplate></asp:LoginView>
        <asp:Label ID="Label1" runat="server" Text="Оберіть підлокацію: "></asp:Label>
            <asp:DropDownList ID="DropDownList1" class="form-select" AutoPostBack="true" runat="server" DataSourceID="SqlDataSource2" DataTextField="nameSubLocation" DataValueField="nameSubLocation" AppendDataBoundItems="true">
            <asp:ListItem Text="Всі" Value="%" />
        </asp:DropDownList>
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:DSTConnectionString %>" SelectCommand="SELECT [nameSubLocation] FROM [SubLocations]"></asp:SqlDataSource>
        <br />
        <asp:Label ID="Label3" runat="server" Text="Оберіть персонажа: "></asp:Label>
            <asp:DropDownList ID="DropDownList2" class="form-select" AutoPostBack="True" runat="server" DataSourceID="SqlDataSource3" DataTextField="nameHero" DataValueField="nameHero" AppendDataBoundItems="True">
            <asp:ListItem Text="Всі" Value="%" />
        </asp:DropDownList>
        <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:DSTConnectionString %>" SelectCommand="SELECT [nameHero] FROM [Heros]"></asp:SqlDataSource>
        <br />
        <asp:Label ID="Label4" runat="server" Text="Оберіть поточний квест: "></asp:Label>
            <asp:DropDownList ID="DropDownList3" class="form-select" AutoPostBack="True" runat="server" DataSourceID="SqlDataSource4" DataTextField="nameQuest" DataValueField="nameQuest" AppendDataBoundItems="True">
            <asp:ListItem Text="Всі" Value="%" />
        </asp:DropDownList>
        <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:DSTConnectionString %>" SelectCommand="SELECT [nameQuest] FROM [Quests]"></asp:SqlDataSource>
        <br />
        <div>
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="15" DataKeyNames="idPlayer" DataSourceID="SqlDataSource1" ForeColor="Black" GridLines="Horizontal" AllowPaging="True" AllowSorting="True" HorizontalAlign="Center" OnRowDeleting="GridView1_RowDeleting" OnRowEditing="GridView1_RowEditing" ShowHeaderWhenEmpty="True">
                <Columns>
                    <asp:BoundField DataField="idPlayer" HeaderText="№" ReadOnly="True" SortExpression="idPlayer" />
                    <asp:BoundField DataField="namePlayer" HeaderText="Ім'я" SortExpression="namePlayer" />
                    <asp:BoundField DataField="hpPlayer" HeaderText="Очки здоров'я" SortExpression="hpPlayer" />
                    <asp:BoundField DataField="damagePlayer" HeaderText="Нанесення пошкодження" SortExpression="damagePlayer" />
                    <asp:BoundField DataField="levelPlayer" HeaderText="Рівень" SortExpression="levelPlayer" />
                    <asp:BoundField DataField="nameSublocation" HeaderText="Поточна підлокація" SortExpression="nameSublocation" />
                    <asp:BoundField DataField="nameHero" HeaderText="Персонаж" SortExpression="nameHero" />
                    <asp:BoundField DataField="nameQuest" HeaderText="Поточний квест" SortExpression="nameQuest" />
                    <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
                </Columns>
                <FooterStyle BackColor="#333333" ForeColor="Black" />
                <HeaderStyle BackColor="#28242c" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="White" ForeColor="Black" Font-Size="X-Large" HorizontalAlign="Center" />
                <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#E5E5E5" />
                <SortedAscendingHeaderStyle BackColor="#333333" />
                <SortedDescendingCellStyle BackColor="#E5E5E5" />
                <SortedDescendingHeaderStyle BackColor="#333333" />
            </asp:GridView>
            <asp:SqlDataSource ID="SqlDataSource1" CancelSelectOnNullParameter="False" runat="server" ConnectionString="<%$ ConnectionStrings:DSTConnectionString %>" SelectCommand="SELECT * FROM [StructPlayer] WHERE (([namePlayer] LIKE '%' + @namePlayer + '%') AND ([nameSublocation] LIKE '%' + @nameSublocation + '%') AND ([nameHero] LIKE '%' + @nameHero + '%') AND ([nameQuest] LIKE '%' + @nameQuest + '%'))" DeleteCommand="DELETE FROM [Player] WHERE idPlayer = @idPlayer" UpdateCommand="UPDATE Player SET itemsPlayer = @itemsPlayer, hpPlayer = @hpPlayer, damagePlayer = @damagePlayer, levelPlayer = @levelPlayer, idSubLocation = @idSubLocation, idHero = @idHero, idQuest = @idQuest WHERE (idPlayer = @idPlayer)">
                <SelectParameters>
                    <asp:ControlParameter ControlID="TextBox1" DefaultValue="%" Name="namePlayer" PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="DropDownList1" DefaultValue="%" Name="nameSublocation" PropertyName="SelectedValue" Type="String" />
                    <asp:ControlParameter ControlID="DropDownList2" DefaultValue="%" Name="nameHero" PropertyName="SelectedValue" Type="String" />
                    <asp:ControlParameter ControlID="DropDownList3" DefaultValue="%" Name="nameQuest" PropertyName="SelectedValue" Type="String" />
                </SelectParameters>
                
            </asp:SqlDataSource>
        </div><br /><br />
                   </form>
            </div>
</body>
</html>
