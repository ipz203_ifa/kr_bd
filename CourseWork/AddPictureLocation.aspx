﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AddPictureLocation.aspx.cs" Inherits="AddPictureLocation" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>AddNewPictureLocation</title>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous" />
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
</head>
<body>
   <div class="container">
    <nav class="navbar navbar-dark bg-dark justify-content-between">
      <a class="navbar-brand mx-5" style="color: white; font-size: xx-large; font-weight: bold" href="Index">Don't Starve Together</a>
    </nav>
       <br />
       <nav aria-label="breadcrumb">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="Index">Головна</a></li>
                    <li class="breadcrumb-item"><a href="SubLocationsView">Підлокації</a></li>
                    <li class="breadcrumb-item active" aria-current="AddPictureLocation">Додати мапу локації</li>
                  </ol>
                </nav>
       <br />

        <div class="col-12">
        <form id="form1" runat="server">
            <div class="form-group col-6">
                <% if (Context.GetOwinContext().Authentication.User.Identity.IsAuthenticated == true) { %>
                <h3>Додати мапу локації</h3>
                <br />
                <asp:Label ID="Label1" runat="server" Text="Назва локації: "></asp:Label>
                <asp:TextBox ID="TextBox1" runat="server" class="form-control"></asp:TextBox>
                <br />
                <asp:Label ID="Label2" runat="server" Text="Оберіть файл: "></asp:Label>
                
                <input id="oFile" type="file" runat="server" name="oFile" class="form-control">
                <asp:Panel ID="frmConfirmation" Visible="False" class="form-control" Runat="server">
                     <asp:Label id="lblUploadResult" Runat="server"></asp:Label>
                </asp:Panel>
                
                <br />
                <br />
                <asp:Button ID="Button1" runat="server" type="submit" class="btn btn-dark" Text="Завантажити" OnClick="Button1_Click" />
            <%} %>
            <% else { %>
             <h3 class="alert alert-danger">Недостатньо прав</h3>
            <%} %>
            </div>
            <br /><br />
             
        </form>
     </div>
     </div>
</body>
</html>
