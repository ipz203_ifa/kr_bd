﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class EditPlayer : System.Web.UI.Page
{
    System.Data.SqlClient.SqlConnection sqlConnection = new System.Data.SqlClient.SqlConnection(@"Data Source = FRANC; Initial Catalog = DST; Integrated Security = True");

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public static void MessageBox(System.Web.UI.Page page, string strMsg)
    {
        ScriptManager.RegisterClientScriptBlock(page, page.GetType(), "alertMessage", "alert('" + strMsg + "')", true);
    }

    void GetCurrentValues()
    {
        System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
        cmd.CommandText = "SELECT * FROM [Player] WHERE idPlayer = @idPlayer";
        cmd.Connection = sqlConnection;
        sqlConnection.Open();
        SqlParameter idPlayer = new SqlParameter("@idPlayer", Request.QueryString["id"].ToString());
        cmd.Parameters.Add(idPlayer);
        SqlDataReader dr = cmd.ExecuteReader();
        if (dr.Read() == true)
        {
            TextBox1.Text = dr.GetSqlValue(1).ToString();
            TextBox2.Text = dr.GetSqlValue(3).ToString();
            TextBox4.SelectedValue = dr.GetSqlValue(5).ToString();
            TextBox5.SelectedValue = dr.GetSqlValue(6).ToString();
            TextBox6.SelectedValue = dr.GetSqlValue(7).ToString();
        }
        sqlConnection.Close();
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        MessageBox(this, TextBox5.SelectedItem.Value);
        System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
        cmd.CommandType = System.Data.CommandType.Text;
        cmd.CommandText = "UPDATE [Player] SET itemsPlayer = @itemsPlayer, hpPlayer = @hpPlayer, damagePlayer = @damagePlayer, levelPlayer = @levelPlayer, idSubLocation = @idSubLocation, idHero = @idHero, idQuest = @idQuest WHERE idPlayer = @idPlayer";
        cmd.Connection = sqlConnection;
        sqlConnection.Open();
        var hp = Int32.Parse(TextBox2.Text) * 5;
        var dam = Int32.Parse(TextBox2.Text) * 2;
        SqlParameter itemsPlayer = new SqlParameter("@itemsPlayer", TextBox1.Text);
        SqlParameter hpPlayer = new SqlParameter("@hpPlayer", hp);
        SqlParameter levelPlayer = new SqlParameter("@levelPlayer", TextBox2.Text);
        SqlParameter damagePlayer = new SqlParameter("@damagePlayer", dam);
        SqlParameter idSubLocation = new SqlParameter("@idSubLocation", TextBox4.SelectedItem.Value);
        SqlParameter idHero = new SqlParameter("@idHero", TextBox5.SelectedItem.Value);
        SqlParameter idQuest = new SqlParameter("@idQuest", TextBox6.SelectedItem.Value);
        SqlParameter idPlayer = new SqlParameter("@idPlayer", Request.QueryString["id"]);
        cmd.Parameters.Add(itemsPlayer);
        cmd.Parameters.Add(hpPlayer);
        cmd.Parameters.Add(levelPlayer);
        cmd.Parameters.Add(damagePlayer);
        cmd.Parameters.Add(idSubLocation);
        cmd.Parameters.Add(idHero);
        cmd.Parameters.Add(idQuest);
        cmd.Parameters.Add(idPlayer);
        cmd.ExecuteNonQuery();
        sqlConnection.Close();
        Response.Redirect("PlayerView.aspx");
    }

    protected void SqlDataSource1_Init(object sender, EventArgs e)
    {
        GetCurrentValues();
    }
}