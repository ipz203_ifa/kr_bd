﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AddSubLocation : System.Web.UI.Page
{
    System.Data.SqlClient.SqlConnection sqlConnection = new System.Data.SqlClient.SqlConnection(@"Data Source = FRANC; Initial Catalog = DST; Integrated Security = True");

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
        cmd.CommandType = System.Data.CommandType.Text;
        cmd.CommandText = "INSERT [SubLocations] (idLocation, nameSubLocation, descSubLocation) " +
            "VALUES(@idLocation, @nameSubLocation, @descSubLocation)";
        cmd.Connection = sqlConnection;
        sqlConnection.Open();
        SqlParameter idLocation = new SqlParameter("@idLocation", TextBox1.SelectedItem.Value);
        SqlParameter nameSubLocation = new SqlParameter("@nameSubLocation", TextBox2.Text);
        SqlParameter descSubLocation = new SqlParameter("@descSubLocation", TextBox4.Text);

        cmd.Parameters.Add(idLocation);
        cmd.Parameters.Add(nameSubLocation);
        cmd.Parameters.Add(descSubLocation);

        if (cmd.ExecuteNonQuery() > 0)
            Response.Redirect("SubLocationsView.aspx");
        cmd.ExecuteNonQuery();
        sqlConnection.Close();
    }
}